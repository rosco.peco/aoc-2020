INPUT:
    dc.b "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd", 13
    dc.b "byr:1937 iyr:2017 cid:147 hgt:183cm", 13
    dc.b "", 13
    dc.b "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884", 13
    dc.b "hcl:#cfa07d byr:1929", 13
    dc.b "", 13
    dc.b "hcl:#ae17e1 iyr:2013", 13
    dc.b "eyr:2024", 13
    dc.b "ecl:brn pid:760753108 byr:1931", 13
    dc.b "hgt:179cm", 13
    dc.b "", 13
    dc.b "hcl:#cfa07d eyr:2025 pid:166559648", 13
    dc.b "iyr:2011 ecl:brn hgt:59in", 13,0
INPUTEND:
