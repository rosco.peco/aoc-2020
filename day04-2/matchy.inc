; *************************************************************
; Advent Of Code 2020 Day 04 - Part 2
; Copyright (c) 2020 Ross Bamford (roscopeco@gmail.com)
;
; Matchy McMatchface - a simple NFA matching engine for 
;                      AoC 2020 Day 4 (and likely beyond...)
; *************************************************************

; Structure of a NFA 
NFA_COUNT equ   0   ; Number of states
NFA_ST    equ   2   ; First state

; Structure of an individual state
STATE_OP  equ   0   ; Opcode
STATE_T0  equ   2   ; Target state 0
STATE_T1  equ   4   ; Target state 1 (used by OP_CHOICE)
STATE_C0  equ   6   ; Data 0 (byte)
STATE_C1  equ   7   ; Data 1 (byte, used by OP_RANGE)
STATE_SZ  equ   8   ; Size of a state is 8 bytes

CLASS_BM  equ   8   ; OP_CLASS bitmap starts at offset 8

; These must be multiples of two to save us a lsl.w in the 
; jump table (in matchy.S).
OP_STOP   equ   $0  ; Fin
OP_MATCH  equ   $2  ; Match single character
OP_CHOICE equ   $4  ; Choice (with backtracking)
OP_CAPON  equ   $6  ; Start capturing
OP_CAPOFF equ   $8  ; Stop capturing
OP_ANY    equ   $A  ; Match any character
OP_RANGE  equ   $C  ; Match range (inclusive)
OP_NRANGE equ   $E  ; Inverse of OP_RANGE
OP_CLASS  equ   $10 ; Character class (see note, below!)

; N.B. OP_CLASS is special, in that it actually requires five
; consecutive states. The four states following the actual 
; OP_CLASS state are used to store the 256-bit bitmap for the 
; class (one bit for each of the 256 possible ASCII values). 
;
; For this operation, the C0 and C1 store flags that affect
; the operation of the class. Currently, only bit 0 of C0 is
; used, and indicates that the class match should be inverted.
;
; It is important that no other state directly reference the
; additional states of the OP_CLASS, or a crash is likely to 
; occur.
;
; And yes, this means that multi-byte character sets are
; not supported. They aren't supported in the rest of the 
; engine either though, so no biggie.
;
; See examples.

