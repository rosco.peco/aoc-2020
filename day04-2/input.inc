INPUT:
    dc.b "eyr:2028 iyr:2016 byr:1995 ecl:oth", 13
    dc.b "pid:543685203 hcl:#c0946f", 13
    dc.b "hgt:152cm", 13
    dc.b "cid:252", 13
    dc.b "", 13
    dc.b "hcl:#733820 hgt:155cm", 13
    dc.b "iyr:2013 byr:1989 pid:728471979", 13
    dc.b "ecl:grn eyr:2022", 13
    dc.b "", 13
    dc.b "hgt:171cm", 13
    dc.b "iyr:2013 pid:214368857 hcl:#cfa07d byr:1986 eyr:2028 ecl:grn", 13
    dc.b "", 13
    dc.b "hgt:167cm cid:210 ecl:brn pid:429131951 hcl:#cfa07d eyr:2029 iyr:2010", 13
    dc.b "byr:1945", 13
    dc.b "", 13
    dc.b "hcl:#888785 iyr:2015", 13
    dc.b "hgt:170cm pid:893805464 ecl:amb byr:1966 eyr:2028", 13
    dc.b "", 13
    dc.b "hgt:170cm ecl:amb", 13
    dc.b "hcl:#c0946f eyr:2020 iyr:2016 pid:725010548", 13
    dc.b "byr:1928", 13
    dc.b "", 13
    dc.b "byr:1999 hcl:#888785", 13
    dc.b "eyr:2026", 13
    dc.b "ecl:hzl", 13
    dc.b "iyr:2016 hgt:193cm pid:170608679", 13
    dc.b "", 13
    dc.b "eyr:2024 iyr:2016 hcl:#cfa07d ecl:grn byr:2001 pid:391942873 cid:104 hgt:164cm", 13
    dc.b "", 13
    dc.b "iyr:2019", 13
    dc.b "eyr:2025 pid:138912840 byr:1996", 13
    dc.b "hgt:166cm", 13
    dc.b "hcl:#888785 ecl:grn", 13
    dc.b "", 13
    dc.b "iyr:2023 hcl:a58381 pid:#401a29 eyr:1940", 13
    dc.b "byr:1920", 13
    dc.b "ecl:utc hgt:183cm", 13
    dc.b "", 13
    dc.b "pid:493510244 ecl:gry hgt:153cm byr:1950 cid:181 eyr:2028", 13
    dc.b "hcl:#ceb3a1", 13
    dc.b "iyr:2020", 13
    dc.b "", 13
    dc.b "iyr:2018 pid:074340974 hgt:182cm", 13
    dc.b "hcl:#866857 byr:1988 ecl:hzl eyr:2023", 13
    dc.b "", 13
    dc.b "hcl:#866857 ecl:oth byr:1977 iyr:2014 hgt:180cm pid:860745884", 13
    dc.b "eyr:2023", 13
    dc.b "", 13
    dc.b "eyr:2026 pid:815594641", 13
    dc.b "ecl:gry iyr:2012 byr:1992 hgt:161cm hcl:#b6652a", 13
    dc.b "", 13
    dc.b "ecl:gry cid:338 eyr:2021 pid:777099878 hgt:193cm hcl:#efcc98", 13
    dc.b "byr:1945", 13
    dc.b "iyr:2015", 13
    dc.b "", 13
    dc.b "iyr:2016 byr:1934 hcl:#b6652a", 13
    dc.b "hgt:162cm ecl:hzl", 13
    dc.b "cid:296", 13
    dc.b "pid:742610207", 13
    dc.b "eyr:2022", 13
    dc.b "", 13
    dc.b "ecl:#ba3242", 13
    dc.b "hgt:80 byr:1931", 13
    dc.b "pid:550004054 iyr:1949 eyr:1944 hcl:fb3859", 13
    dc.b "", 13
    dc.b "ecl:amb eyr:2024", 13
    dc.b "byr:1965 iyr:2010 pid:094059049", 13
    dc.b "hcl:#fffffd", 13
    dc.b "hgt:168cm", 13
    dc.b "", 13
    dc.b "pid:159cm", 13
    dc.b "iyr:1923 eyr:2032 hcl:701107 cid:343 ecl:gmt byr:2010", 13
    dc.b "hgt:177cm", 13
    dc.b "", 13
    dc.b "eyr:2021", 13
    dc.b "ecl:grn byr:1991", 13
    dc.b "hcl:#fffffd hgt:167cm pid:243218792 iyr:2019", 13
    dc.b "", 13
    dc.b "hgt:157cm byr:2017 ecl:grn iyr:2012", 13
    dc.b "eyr:2030 hcl:#18171d pid:173cm", 13
    dc.b "", 13
    dc.b "pid:260101979 hgt:187cm eyr:2033 ecl:lzr", 13
    dc.b "byr:2020 hcl:1058ce cid:133 iyr:2012", 13
    dc.b "", 13
    dc.b "hcl:#7d3b0c", 13
    dc.b "pid:307828343 byr:2001", 13
    dc.b "cid:317 iyr:2013", 13
    dc.b "eyr:2029", 13
    dc.b "", 13
    dc.b "pid:472940417 eyr:1960", 13
    dc.b "hgt:181cm hcl:#c0946f cid:269", 13
    dc.b "byr:2014", 13
    dc.b "iyr:1956", 13
    dc.b "", 13
    dc.b "hcl:#18171d eyr:2021 byr:2001 pid:421443124", 13
    dc.b "ecl:brn iyr:2020 hgt:156cm", 13
    dc.b "", 13
    dc.b "cid:347 hgt:60in pid:359783692 byr:1932", 13
    dc.b "ecl:hzl", 13
    dc.b "eyr:2023", 13
    dc.b "hcl:#888785 iyr:2019", 13
    dc.b "", 13
    dc.b "pid:230915137", 13
    dc.b "byr:1999", 13
    dc.b "iyr:2011 eyr:2020 hcl:#7d3b0c ecl:hzl", 13
    dc.b "hgt:164cm", 13
    dc.b "", 13
    dc.b "iyr:1989", 13
    dc.b "byr:2008", 13
    dc.b "hgt:154cm", 13
    dc.b "eyr:2028 pid:280298169", 13
    dc.b "cid:208", 13
    dc.b "ecl:oth", 13
    dc.b "", 13
    dc.b "byr:1954 iyr:2017", 13
    dc.b "ecl:hzl", 13
    dc.b "eyr:2026", 13
    dc.b "pid:966957581 hgt:175cm hcl:#18171d", 13
    dc.b "", 13
    dc.b "pid:308053355 hgt:192cm eyr:2022 ecl:amb cid:146 iyr:2015", 13
    dc.b "byr:1991 hcl:#c0946f", 13
    dc.b "", 13
    dc.b "hcl:#a97842 pid:244441133 iyr:2019", 13
    dc.b "hgt:182cm", 13
    dc.b "ecl:amb cid:172 byr:1973 eyr:2029", 13
    dc.b "", 13
    dc.b "iyr:2017", 13
    dc.b "byr:1985 cid:215", 13
    dc.b "ecl:blu hcl:#623a2f hgt:160cm pid:157856689 eyr:2030", 13
    dc.b "", 13
    dc.b "eyr:2027 ecl:#d72f9b hgt:162cm", 13
    dc.b "iyr:2018 hcl:#a97842", 13
    dc.b "byr:1945", 13
    dc.b "pid:131243258", 13
    dc.b "", 13
    dc.b "hcl:#b3f2f0 pid:204254353 cid:169 eyr:2020", 13
    dc.b "iyr:2013 hgt:172cm ecl:blu byr:1950", 13
    dc.b "", 13
    dc.b "byr:1957 hcl:#c0946f hgt:152cm ecl:blu eyr:2027 pid:325917033", 13
    dc.b "iyr:2010", 13
    dc.b "", 13
    dc.b "ecl:oth byr:1950 hgt:166cm pid:007352351", 13
    dc.b "hcl:#b6652a", 13
    dc.b "iyr:2020", 13
    dc.b "eyr:2024", 13
    dc.b "", 13
    dc.b "hgt:165 eyr:2030 iyr:2027", 13
    dc.b "ecl:#1a34f1 pid:2894591864 byr:2024 hcl:z", 13
    dc.b "", 13
    dc.b "byr:1971 ecl:oth", 13
    dc.b "hgt:163cm eyr:2021 pid:040443396", 13
    dc.b "", 13
    dc.b "hgt:177cm", 13
    dc.b "byr:1955 pid:585735590 iyr:2010 ecl:grn eyr:2024", 13
    dc.b "hcl:#602927", 13
    dc.b "", 13
    dc.b "cid:74", 13
    dc.b "iyr:2010", 13
    dc.b "pid:014378493 hgt:174cm eyr:2020", 13
    dc.b "ecl:grn byr:1944", 13
    dc.b "", 13
    dc.b "pid:404141049", 13
    dc.b "byr:1947 ecl:blu hgt:170cm iyr:2011", 13
    dc.b "eyr:2028", 13
    dc.b "hcl:#cfa07d", 13
    dc.b "", 13
    dc.b "ecl:hzl byr:1938 pid:235085606 cid:180 hcl:8fb74c eyr:2021 hgt:73 iyr:2015", 13
    dc.b "", 13
    dc.b "pid:860077423 ecl:gry", 13
    dc.b "hcl:#3e845b", 13
    dc.b "hgt:167cm byr:1933 iyr:2016 eyr:2021", 13
    dc.b "", 13
    dc.b "hcl:#733820 hgt:66in eyr:1920", 13
    dc.b "ecl:oth byr:1941 pid:979460474 iyr:2010", 13
    dc.b "cid:247", 13
    dc.b "", 13
    dc.b "hcl:#cfa07d ecl:#13bd36 hgt:193cm eyr:2027 pid:181cm byr:1952 iyr:1951", 13
    dc.b "", 13
    dc.b "ecl:brn hcl:#602927", 13
    dc.b "hgt:161cm", 13
    dc.b "eyr:2027 pid:822749462 byr:1946", 13
    dc.b "iyr:2014", 13
    dc.b "", 13
    dc.b "byr:2013", 13
    dc.b "iyr:2021 ecl:zzz eyr:2032 hgt:193in hcl:#a97842 pid:163cm", 13
    dc.b "", 13
    dc.b "eyr:2029 cid:140", 13
    dc.b "byr:1984", 13
    dc.b "iyr:2018 hgt:187cm hcl:#b6652a pid:910674579", 13
    dc.b "", 13
    dc.b "ecl:hzl hgt:173cm pid:096026282", 13
    dc.b "iyr:2014 byr:1956", 13
    dc.b "eyr:2029 hcl:#866857", 13
    dc.b "", 13
    dc.b "eyr:2024 iyr:2019 pid:301205967", 13
    dc.b "cid:276 byr:1957 hcl:#3fec29 ecl:gry hgt:165cm", 13
    dc.b "", 13
    dc.b "iyr:2013 ecl:oth hgt:177cm hcl:#6b5442 eyr:2021 byr:1962 pid:006347857", 13
    dc.b "", 13
    dc.b "ecl:grt byr:1983 hcl:#cfa07d", 13
    dc.b "hgt:163cm", 13
    dc.b "eyr:1979", 13
    dc.b "iyr:1958 pid:796395720", 13
    dc.b "", 13
    dc.b "iyr:2011 pid:415403544 hcl:#c0946f byr:1990 ecl:oth eyr:2023 hgt:73in", 13
    dc.b "cid:107", 13
    dc.b "", 13
    dc.b "hgt:166cm eyr:2029 iyr:2015", 13
    dc.b "hcl:#c0946f ecl:brn", 13
    dc.b "byr:1964", 13
    dc.b "pid:469449137", 13
    dc.b "", 13
    dc.b "eyr:2023", 13
    dc.b "byr:1969 iyr:2010 hgt:163cm hcl:#a97842 pid:570942274", 13
    dc.b "ecl:blu", 13
    dc.b "", 13
    dc.b "hcl:#623a2f", 13
    dc.b "ecl:brn hgt:183cm pid:524675399", 13
    dc.b "eyr:2020 iyr:2012 byr:1981", 13
    dc.b "", 13
    dc.b "iyr:2017 hcl:#fffffd eyr:2026", 13
    dc.b "ecl:gry byr:1979 hgt:152cm pid:505790864", 13
    dc.b "", 13
    dc.b "hgt:68in", 13
    dc.b "hcl:#c0946f iyr:2012", 13
    dc.b "eyr:2023 pid:933562997 byr:1993", 13
    dc.b "ecl:grn", 13
    dc.b "", 13
    dc.b "pid:267705171", 13
    dc.b "hgt:166cm byr:1970 iyr:2019 hcl:#341e13 ecl:oth", 13
    dc.b "eyr:2030", 13
    dc.b "", 13
    dc.b "ecl:brn byr:1972 eyr:2026 pid:774637408 hgt:189cm iyr:2015 hcl:#341e13", 13
    dc.b "", 13
    dc.b "hgt:175cm eyr:2026 byr:2001 iyr:2020", 13
    dc.b "hcl:#733820 ecl:blu pid:686996160", 13
    dc.b "", 13
    dc.b "hgt:190cm hcl:#c0946f pid:228444464 byr:1987", 13
    dc.b "iyr:2020 eyr:2030", 13
    dc.b "ecl:blu", 13
    dc.b "", 13
    dc.b "byr:1990 hgt:179cm", 13
    dc.b "pid:885359438 eyr:2028 iyr:2010 ecl:amb", 13
    dc.b "hcl:#67067e", 13
    dc.b "", 13
    dc.b "byr:1945 hcl:#866857 eyr:2022 iyr:2019", 13
    dc.b "pid:708146656 cid:65", 13
    dc.b "hgt:172cm ecl:brn", 13
    dc.b "", 13
    dc.b "ecl:hzl hgt:191cm", 13
    dc.b "cid:260 pid:010716679 iyr:2011 eyr:2029 byr:1920 hcl:#efcc98", 13
    dc.b "", 13
    dc.b "iyr:2012", 13
    dc.b "cid:313 pid:264894705 byr:1951 hcl:#733820 eyr:2030 ecl:blu", 13
    dc.b "hgt:178cm", 13
    dc.b "", 13
    dc.b "eyr:2027 pid:790510379", 13
    dc.b "iyr:2013", 13
    dc.b "ecl:amb", 13
    dc.b "hgt:186cm", 13
    dc.b "hcl:#866857", 13
    dc.b "byr:1926", 13
    dc.b "", 13
    dc.b "pid:535750794 hgt:191cm iyr:2016 hcl:#a97842 eyr:2029", 13
    dc.b "ecl:hzl byr:1923", 13
    dc.b "", 13
    dc.b "byr:2023 pid:#eb4c2a iyr:1939 ecl:grn hcl:06d729 hgt:73 eyr:2038", 13
    dc.b "", 13
    dc.b "pid:792365221 iyr:2013 ecl:oth", 13
    dc.b "byr:1997", 13
    dc.b "hgt:170cm hcl:#efcc98", 13
    dc.b "eyr:2022", 13
    dc.b "", 13
    dc.b "hgt:192cm pid:874141668", 13
    dc.b "byr:1957 iyr:2015", 13
    dc.b "ecl:gry", 13
    dc.b "", 13
    dc.b "hcl:#b6652a pid:770238761 eyr:2029 byr:1934 iyr:2013", 13
    dc.b "ecl:blu cid:177", 13
    dc.b "hgt:184cm", 13
    dc.b "", 13
    dc.b "ecl:hzl eyr:2024 hgt:72in pid:546439165", 13
    dc.b "iyr:2013", 13
    dc.b "hcl:#c0946f cid:223 byr:1989", 13
    dc.b "", 13
    dc.b "byr:1985", 13
    dc.b "ecl:utc pid:#ff1cbf", 13
    dc.b "iyr:2018 hcl:#866857 hgt:169cm eyr:2026 cid:194", 13
    dc.b "", 13
    dc.b "hgt:189cm", 13
    dc.b "eyr:2026 pid:120642045 ecl:blu", 13
    dc.b "hcl:#602927 cid:177", 13
    dc.b "byr:1954 iyr:2012", 13
    dc.b "", 13
    dc.b "pid:314624973", 13
    dc.b "byr:1959 iyr:2015 hcl:#c0946f ecl:grn", 13
    dc.b "eyr:2027 cid:349 hgt:156cm", 13
    dc.b "", 13
    dc.b "byr:1978", 13
    dc.b "iyr:2020 hgt:150cm cid:266 eyr:2026", 13
    dc.b "pid:443912835 hcl:#b6652a", 13
    dc.b "", 13
    dc.b "hgt:174cm byr:1974 pid:729198828", 13
    dc.b "ecl:brn iyr:2014", 13
    dc.b "hcl:#18171d eyr:2027", 13
    dc.b "", 13
    dc.b "pid:472891001 ecl:xry", 13
    dc.b "hgt:96 hcl:1b816a iyr:1954", 13
    dc.b "byr:2015 eyr:2037", 13
    dc.b "", 13
    dc.b "byr:1966 eyr:2022", 13
    dc.b "iyr:2014", 13
    dc.b "pid:848187688 hcl:#602927 ecl:gry hgt:152cm", 13
    dc.b "", 13
    dc.b "hgt:129 eyr:2037 cid:61 iyr:2009 byr:2027 hcl:#c0946f", 13
    dc.b "pid:3569865", 13
    dc.b "ecl:#4e3d72", 13
    dc.b "", 13
    dc.b "ecl:gry", 13
    dc.b "eyr:2021 pid:234525998 byr:1964 hgt:168cm cid:140", 13
    dc.b "hcl:#7d3b0c iyr:2013", 13
    dc.b "", 13
    dc.b "ecl:xry", 13
    dc.b "cid:86", 13
    dc.b "hgt:172in", 13
    dc.b "byr:1972", 13
    dc.b "iyr:2015 hcl:#7d3b0c pid:833809421 eyr:2030", 13
    dc.b "", 13
    dc.b "pid:444365280 hgt:72in", 13
    dc.b "ecl:brn", 13
    dc.b "hcl:#b6652a byr:1985 eyr:2027 iyr:2012", 13
    dc.b "", 13
    dc.b "iyr:2010 byr:2013 hgt:181cm eyr:2021", 13
    dc.b "pid:072317444", 13
    dc.b "ecl:oth hcl:#866857", 13
    dc.b "cid:118", 13
    dc.b "", 13
    dc.b "pid:4354408888 iyr:2012", 13
    dc.b "hcl:#b6652a cid:104", 13
    dc.b "hgt:96 eyr:2020", 13
    dc.b "byr:1933 ecl:amb", 13
    dc.b "", 13
    dc.b "eyr:2023 ecl:gry hcl:#a97842 pid:287719484 byr:1994", 13
    dc.b "iyr:2011 hgt:163cm cid:299", 13
    dc.b "", 13
    dc.b "byr:1932", 13
    dc.b "hgt:170cm", 13
    dc.b "iyr:2014 pid:777844412 eyr:2040 hcl:#cfa07d ecl:brn", 13
    dc.b "", 13
    dc.b "cid:160 hgt:191cm eyr:2020 iyr:2012", 13
    dc.b "ecl:brn byr:1981 pid:077027782", 13
    dc.b "", 13
    dc.b "cid:182 hgt:176cm hcl:#7d3b0c", 13
    dc.b "eyr:2030 ecl:blu pid:096742425 iyr:2010 byr:1963", 13
    dc.b "", 13
    dc.b "byr:2010 cid:337 hcl:z pid:525126586 iyr:2010 hgt:73cm eyr:2040 ecl:blu", 13
    dc.b "", 13
    dc.b "ecl:gry", 13
    dc.b "iyr:2017", 13
    dc.b "hgt:185cm hcl:#6b5442 byr:1993", 13
    dc.b "eyr:2029 pid:366083139 cid:343", 13
    dc.b "", 13
    dc.b "eyr:2028 ecl:amb", 13
    dc.b "pid:878658841 byr:1960 hgt:179cm hcl:#18171d iyr:2010", 13
    dc.b "", 13
    dc.b "pid:537309261 iyr:2015 hgt:187cm", 13
    dc.b "hcl:#4fe831 eyr:2026", 13
    dc.b "ecl:blu byr:1982", 13
    dc.b "", 13
    dc.b "ecl:brn hgt:163cm", 13
    dc.b "eyr:2021 hcl:#6b5442 byr:1979 iyr:2013 pid:924759517", 13
    dc.b "", 13
    dc.b "pid:683651053 hcl:#179c55", 13
    dc.b "ecl:blu byr:1989 hgt:190cm", 13
    dc.b "iyr:2016", 13
    dc.b "eyr:2030", 13
    dc.b "", 13
    dc.b "ecl:grn", 13
    dc.b "iyr:2016 hcl:#b6652a", 13
    dc.b "byr:1994 eyr:2020 pid:448424292 hgt:174cm", 13
    dc.b "", 13
    dc.b "hgt:157cm", 13
    dc.b "ecl:grn", 13
    dc.b "byr:2000", 13
    dc.b "pid:734707993 hcl:#341e13 iyr:2020", 13
    dc.b "", 13
    dc.b "hcl:#341e13 hgt:156cm iyr:2020 pid:299213638", 13
    dc.b "byr:1947 ecl:hzl eyr:2023", 13
    dc.b "", 13
    dc.b "hgt:193cm hcl:#b6652a iyr:2014 ecl:hzl byr:1947 eyr:2025", 13
    dc.b "pid:044486467", 13
    dc.b "", 13
    dc.b "byr:1975", 13
    dc.b "hgt:159cm", 13
    dc.b "ecl:grn pid:318489576 eyr:2029 hcl:#6b5442", 13
    dc.b "iyr:2020", 13
    dc.b "", 13
    dc.b "iyr:2018 pid:512971930", 13
    dc.b "hcl:#888785 byr:1966 eyr:2024 hgt:158cm", 13
    dc.b "cid:100 ecl:gry", 13
    dc.b "", 13
    dc.b "ecl:amb eyr:2030 hgt:171cm hcl:#efcc98 pid:800921581 cid:339 byr:1980 iyr:2017", 13
    dc.b "", 13
    dc.b "iyr:2019 cid:172", 13
    dc.b "hgt:152cm", 13
    dc.b "eyr:2022 ecl:oth hcl:#602927 byr:1960", 13
    dc.b "", 13
    dc.b "iyr:2019 pid:762312913", 13
    dc.b "eyr:2029", 13
    dc.b "ecl:hzl", 13
    dc.b "hcl:#6b5442", 13
    dc.b "byr:1940", 13
    dc.b "hgt:169cm cid:289", 13
    dc.b "", 13
    dc.b "eyr:2022 ecl:gry byr:1976", 13
    dc.b "iyr:2020 hcl:#733820 hgt:172cm pid:040331561", 13
    dc.b "", 13
    dc.b "hgt:171cm ecl:brn iyr:2013 eyr:2027 byr:1940 hcl:#a6e32a pid:223986941", 13
    dc.b "", 13
    dc.b "hcl:#341e13", 13
    dc.b "eyr:2028 ecl:amb byr:1942", 13
    dc.b "hgt:166cm pid:435382099 iyr:2020", 13
    dc.b "", 13
    dc.b "cid:298 pid:641326891", 13
    dc.b "hgt:155cm hcl:#623a2f ecl:grn byr:1981 eyr:2025", 13
    dc.b "iyr:2010", 13
    dc.b "", 13
    dc.b "iyr:2015 pid:472000322 eyr:2021 byr:1977", 13
    dc.b "ecl:gry hgt:165cm cid:270", 13
    dc.b "", 13
    dc.b "eyr:2027 byr:1956", 13
    dc.b "pid:193087729 hcl:#ceb3a1", 13
    dc.b "cid:213 hgt:193cm ecl:oth", 13
    dc.b "", 13
    dc.b "iyr:2014", 13
    dc.b "byr:1971 cid:96", 13
    dc.b "hgt:74in", 13
    dc.b "pid:136003336", 13
    dc.b "eyr:2020 ecl:hzl hcl:#efcc98", 13
    dc.b "", 13
    dc.b "hcl:z pid:097595072 ecl:amb", 13
    dc.b "iyr:2015 byr:2021", 13
    dc.b "eyr:2039 hgt:188cm", 13
    dc.b "", 13
    dc.b "pid:74823273", 13
    dc.b "hcl:#341e13", 13
    dc.b "cid:166 hgt:182cm byr:2026 iyr:2027 ecl:amb", 13
    dc.b "eyr:2032", 13
    dc.b "", 13
    dc.b "byr:1932 eyr:2022 pid:367248062 hgt:182cm ecl:oth hcl:#c0946f", 13
    dc.b "iyr:2020", 13
    dc.b "", 13
    dc.b "hgt:72cm", 13
    dc.b "iyr:2015 cid:234 byr:2013", 13
    dc.b "ecl:brn pid:9401866358", 13
    dc.b "", 13
    dc.b "pid:022399779 iyr:2010 byr:1969 hcl:#6b5442", 13
    dc.b "ecl:grn eyr:2020", 13
    dc.b "hgt:189cm", 13
    dc.b "", 13
    dc.b "byr:1971 iyr:2011 cid:161 ecl:brn hgt:153cm", 13
    dc.b "eyr:2028 pid:819137905 hcl:#cfa07d", 13
    dc.b "", 13
    dc.b "cid:161 hgt:159cm iyr:2011 pid:815860793 hcl:#a97842 ecl:grn byr:1972 eyr:2027", 13
    dc.b "", 13
    dc.b "ecl:amb", 13
    dc.b "hgt:118 byr:1981 iyr:2019", 13
    dc.b "hcl:#a97842 eyr:2021 pid:270790642", 13
    dc.b "", 13
    dc.b "hcl:#b6652a pid:732272914 eyr:2030 hgt:183cm ecl:hzl", 13
    dc.b "byr:1934", 13
    dc.b "iyr:2018", 13
    dc.b "", 13
    dc.b "eyr:2027", 13
    dc.b "pid:877388498 hcl:#ceb3a1", 13
    dc.b "byr:1925 cid:236 ecl:grn", 13
    dc.b "iyr:2019 hgt:191cm", 13
    dc.b "", 13
    dc.b "eyr:2020 ecl:brn hcl:#fffffd hgt:181cm pid:801311341 byr:1986 iyr:2010", 13
    dc.b "", 13
    dc.b "byr:1925 cid:179 ecl:hzl pid:360641953 eyr:2030", 13
    dc.b "hgt:171in iyr:2015", 13
    dc.b "hcl:#602927", 13
    dc.b "", 13
    dc.b "cid:83 hgt:181cm", 13
    dc.b "eyr:2028 byr:1941 pid:165937945 hcl:#888785 iyr:2014", 13
    dc.b "ecl:grn", 13
    dc.b "", 13
    dc.b "hcl:#a97842 byr:1928", 13
    dc.b "iyr:2013", 13
    dc.b "pid:870072019 hgt:76in", 13
    dc.b "ecl:oth cid:127 eyr:2026", 13
    dc.b "", 13
    dc.b "cid:169", 13
    dc.b "hgt:187cm pid:008180128 iyr:2013 byr:1991 hcl:#7d3b0c ecl:hzl eyr:2026", 13
    dc.b "", 13
    dc.b "ecl:amb", 13
    dc.b "eyr:2027 hgt:155cm pid:586151564 iyr:2010", 13
    dc.b "byr:1949", 13
    dc.b "hcl:#18171d", 13
    dc.b "", 13
    dc.b "hgt:167cm", 13
    dc.b "iyr:2010 byr:1982 ecl:amb", 13
    dc.b "cid:235 pid:557737957 eyr:2020", 13
    dc.b "hcl:#ceb3a1", 13
    dc.b "", 13
    dc.b "ecl:grn byr:1939 hcl:#733820", 13
    dc.b "eyr:2026 pid:993218958 iyr:2010", 13
    dc.b "hgt:150cm", 13
    dc.b "", 13
    dc.b "hgt:68in ecl:blu", 13
    dc.b "byr:1965 iyr:2017 pid:854858050 eyr:2021", 13
    dc.b "", 13
    dc.b "ecl:gry pid:347763159 eyr:2024 iyr:2017 byr:1961", 13
    dc.b "hgt:151cm", 13
    dc.b "hcl:#623a2f", 13
    dc.b "", 13
    dc.b "ecl:utc hcl:#602927", 13
    dc.b "pid:#1408ff byr:1941", 13
    dc.b "cid:82", 13
    dc.b "iyr:2015 hgt:185cm eyr:2028", 13
    dc.b "", 13
    dc.b "iyr:2020 hgt:151cm eyr:2025", 13
    dc.b "byr:1934 hcl:#888785", 13
    dc.b "pid:396545094 ecl:oth", 13
    dc.b "", 13
    dc.b "hgt:153cm", 13
    dc.b "eyr:2028 hcl:#733820 ecl:gry iyr:2019", 13
    dc.b "pid:081352630 byr:1943", 13
    dc.b "", 13
    dc.b "eyr:2030", 13
    dc.b "iyr:2011", 13
    dc.b "ecl:grn pid:313741119", 13
    dc.b "hgt:161cm byr:1946", 13
    dc.b "hcl:#a97842", 13
    dc.b "", 13
    dc.b "byr:1968 ecl:gry", 13
    dc.b "pid:742357550", 13
    dc.b "eyr:2024 hcl:#18171d iyr:2018", 13
    dc.b "hgt:157cm", 13
    dc.b "", 13
    dc.b "pid:387505919", 13
    dc.b "ecl:oth byr:1945", 13
    dc.b "iyr:2014", 13
    dc.b "hgt:190cm hcl:#888785", 13
    dc.b "eyr:2028", 13
    dc.b "", 13
    dc.b "iyr:2017 hgt:175cm", 13
    dc.b "byr:1989 eyr:2022", 13
    dc.b "hcl:#b6652a pid:499016802 ecl:gry cid:136", 13
    dc.b "", 13
    dc.b "pid:490807331 iyr:2016", 13
    dc.b "hcl:#ceb3a1", 13
    dc.b "hgt:150cm eyr:2026", 13
    dc.b "ecl:amb byr:1967", 13
    dc.b "", 13
    dc.b "iyr:2011", 13
    dc.b "hgt:155in", 13
    dc.b "hcl:#ceb3a1 pid:118497416", 13
    dc.b "eyr:2029 byr:2011 ecl:oth", 13
    dc.b "", 13
    dc.b "hcl:03a888 byr:2029", 13
    dc.b "ecl:#6f7292 eyr:1969 iyr:2028 hgt:162cm pid:73551266", 13
    dc.b "", 13
    dc.b "iyr:2016 hgt:182cm", 13
    dc.b "byr:1966 ecl:grn eyr:2022", 13
    dc.b "hcl:#fffffd pid:061720787", 13
    dc.b "", 13
    dc.b "byr:1971 hcl:z", 13
    dc.b "eyr:2035 pid:158cm", 13
    dc.b "ecl:#d3ec19", 13
    dc.b "", 13
    dc.b "hcl:#623a2f hgt:156cm eyr:2028", 13
    dc.b "ecl:brn iyr:2013", 13
    dc.b "byr:1980 pid:112283719", 13
    dc.b "", 13
    dc.b "eyr:2020", 13
    dc.b "byr:1956 iyr:2013", 13
    dc.b "hcl:#6b5442", 13
    dc.b "ecl:grn pid:876589775 hgt:179cm", 13
    dc.b "", 13
    dc.b "hgt:138", 13
    dc.b "byr:2013 eyr:2040 iyr:2028 cid:197 ecl:#8844fd pid:8524414485", 13
    dc.b "hcl:z", 13
    dc.b "", 13
    dc.b "eyr:2040", 13
    dc.b "hgt:173in hcl:z pid:#654654 byr:2016 iyr:2022 ecl:#452d22", 13
    dc.b "", 13
    dc.b "iyr:2012 cid:265 eyr:2021 hgt:192cm", 13
    dc.b "byr:1993 ecl:brn", 13
    dc.b "", 13
    dc.b "eyr:2026 hcl:#888785", 13
    dc.b "hgt:158cm byr:1942", 13
    dc.b "iyr:2015", 13
    dc.b "ecl:amb pid:546984106", 13
    dc.b "", 13
    dc.b "iyr:2019", 13
    dc.b "ecl:hzl", 13
    dc.b "byr:1922 eyr:2028 hgt:172cm", 13
    dc.b "pid:465052232 hcl:#602927", 13
    dc.b "", 13
    dc.b "pid:710362693 eyr:2023", 13
    dc.b "hcl:#c0946f byr:1951 ecl:grn", 13
    dc.b "iyr:2019 hgt:190cm", 13
    dc.b "", 13
    dc.b "iyr:2024 pid:#a08e69", 13
    dc.b "hcl:z byr:1966 ecl:#7b9978 eyr:2035", 13
    dc.b "hgt:69cm", 13
    dc.b "", 13
    dc.b "hcl:#efcc98", 13
    dc.b "pid:164cm", 13
    dc.b "iyr:2010 cid:194 hgt:71cm byr:1923 eyr:2026", 13
    dc.b "", 13
    dc.b "hgt:65in", 13
    dc.b "iyr:2019 byr:1969 pid:466669360 eyr:2022 ecl:brn hcl:#b6652a", 13
    dc.b "", 13
    dc.b "pid:42472559 hcl:#6f5763", 13
    dc.b "eyr:2035", 13
    dc.b "iyr:2014 hgt:154in byr:1939 ecl:grt cid:323", 13
    dc.b "", 13
    dc.b "pid:715680334 hgt:166cm cid:283", 13
    dc.b "byr:1982", 13
    dc.b "iyr:2015 eyr:2030 hcl:#ceb3a1 ecl:grn", 13
    dc.b "", 13
    dc.b "eyr:2018 iyr:2029", 13
    dc.b "ecl:brn", 13
    dc.b "byr:2022 pid:#ff6df1", 13
    dc.b "hcl:z", 13
    dc.b "hgt:68cm", 13
    dc.b "", 13
    dc.b "pid:094541122", 13
    dc.b "eyr:2024 byr:1940", 13
    dc.b "ecl:amb iyr:2019 hgt:64in hcl:#733820", 13
    dc.b "", 13
    dc.b "hgt:163in", 13
    dc.b "eyr:2022 ecl:utc hcl:#ceb3a1 iyr:2028", 13
    dc.b "", 13
    dc.b "ecl:gry pid:53552934", 13
    dc.b "hgt:193 byr:2021", 13
    dc.b "eyr:2028", 13
    dc.b "iyr:2011 cid:98 hcl:90c63f", 13
    dc.b "", 13
    dc.b "eyr:2024 hcl:#cfa07d ecl:brn", 13
    dc.b "iyr:2019 byr:1993 hgt:156cm pid:449484188", 13
    dc.b "", 13
    dc.b "iyr:2020", 13
    dc.b "hgt:164cm hcl:#623a2f", 13
    dc.b "pid:820731743 eyr:2025", 13
    dc.b "byr:1997 ecl:hzl", 13
    dc.b "", 13
    dc.b "hcl:47242b ecl:utc hgt:156", 13
    dc.b "pid:#9a9903 eyr:2030 iyr:1990", 13
    dc.b "byr:2011", 13
    dc.b "", 13
    dc.b "hcl:#602927", 13
    dc.b "hgt:189cm", 13
    dc.b "pid:949021883 iyr:2014 ecl:oth cid:327", 13
    dc.b "eyr:2027 byr:1953", 13
    dc.b "", 13
    dc.b "hgt:189cm cid:301", 13
    dc.b "byr:1982", 13
    dc.b "ecl:grn", 13
    dc.b "eyr:2028 hcl:#733820 pid:796040143 iyr:2015", 13
    dc.b "", 13
    dc.b "cid:169 iyr:2013 pid:355177646 byr:1988", 13
    dc.b "ecl:oth", 13
    dc.b "hcl:#cfa07d", 13
    dc.b "hgt:185cm eyr:2022", 13
    dc.b "", 13
    dc.b "pid:563150261 eyr:2020 ecl:brn byr:1996 hcl:#7d3b0c iyr:2018 hgt:189cm cid:84", 13
    dc.b "", 13
    dc.b "cid:188 eyr:2027", 13
    dc.b "byr:1944", 13
    dc.b "pid:486184923", 13
    dc.b "iyr:2010 hgt:193cm hcl:#341e13 ecl:oth", 13
    dc.b "", 13
    dc.b "iyr:2019", 13
    dc.b "byr:1969 hgt:152cm pid:430698432 ecl:gry hcl:#888785 eyr:2026 cid:293", 13
    dc.b "", 13
    dc.b "ecl:gry", 13
    dc.b "cid:270 hcl:#602927 iyr:2017 hgt:151cm eyr:2029 pid:051398739 byr:1954", 13
    dc.b "", 13
    dc.b "ecl:oth eyr:2030 pid:024655030", 13
    dc.b "hgt:184cm byr:1969", 13
    dc.b "hcl:#18171d", 13
    dc.b "", 13
    dc.b "eyr:2030", 13
    dc.b "pid:899973263 hgt:178cm byr:1987 hcl:#cfa07d iyr:2012", 13
    dc.b "ecl:amb", 13
    dc.b "", 13
    dc.b "iyr:1958 hgt:165cm pid:377677319", 13
    dc.b "ecl:grt eyr:2032 byr:2025", 13
    dc.b "hcl:bbfbe2", 13
    dc.b "", 13
    dc.b "ecl:blu", 13
    dc.b "iyr:2016", 13
    dc.b "hgt:152cm byr:1964", 13
    dc.b "hcl:#c4f777", 13
    dc.b "eyr:2021", 13
    dc.b "pid:044307549 cid:80", 13
    dc.b "", 13
    dc.b "ecl:brn pid:330836320", 13
    dc.b "byr:1963 cid:217 hgt:169cm", 13
    dc.b "eyr:2024", 13
    dc.b "iyr:2019 hcl:#ceb3a1", 13
    dc.b "", 13
    dc.b "byr:1976 eyr:2027", 13
    dc.b "pid:452662874 hgt:192cm ecl:oth iyr:2018 hcl:#602927", 13
    dc.b "", 13
    dc.b "eyr:2027 hgt:183cm ecl:brn iyr:2017 hcl:#341e13 pid:827463598", 13
    dc.b "", 13
    dc.b "ecl:brn pid:930667228 cid:310 iyr:2020", 13
    dc.b "eyr:2027 hgt:160cm byr:1932 hcl:#c0946f", 13
    dc.b "", 13
    dc.b "pid:955804028 byr:1983", 13
    dc.b "hcl:#fffffd", 13
    dc.b "hgt:178cm iyr:2013", 13
    dc.b "eyr:2021 ecl:gry", 13
    dc.b "", 13
    dc.b "hgt:189cm eyr:2021 pid:430243363 iyr:2015 hcl:#ceb3a1", 13
    dc.b "byr:2000 ecl:oth cid:284", 13
    dc.b "", 13
    dc.b "pid:436671537 hcl:#cfa07d iyr:2011 cid:106 hgt:171cm", 13
    dc.b "ecl:blu eyr:2021 byr:1943", 13
    dc.b "", 13
    dc.b "eyr:2028 hgt:169cm", 13
    dc.b "iyr:2015 pid:177443573 byr:1945", 13
    dc.b "hcl:#c0946f ecl:gry", 13
    dc.b "", 13
    dc.b "hcl:#fffffd byr:1995 eyr:2021", 13
    dc.b "ecl:grn", 13
    dc.b "hgt:192cm iyr:2010 pid:754912745", 13
    dc.b "", 13
    dc.b "pid:330882171 iyr:2015 cid:211 ecl:grn byr:1961 eyr:2021 hcl:z", 13
    dc.b "hgt:169cm", 13
    dc.b "", 13
    dc.b "byr:1926 eyr:2029 pid:178633665 cid:141 iyr:2017 hcl:#b99eb9", 13
    dc.b "hgt:178cm ecl:brn", 13
    dc.b "", 13
    dc.b "eyr:2022 ecl:hzl hcl:#cfa07d hgt:168cm iyr:2015", 13
    dc.b "byr:1982 pid:645675448", 13
    dc.b "", 13
    dc.b "ecl:blu byr:1980 hgt:186cm iyr:2010 cid:94 hcl:#c0946f eyr:2027 pid:384440210", 13
    dc.b "", 13
    dc.b "cid:309 hcl:#602927 hgt:192cm eyr:2027 ecl:amb", 13
    dc.b "pid:527932745 iyr:2012 byr:1982", 13
    dc.b "", 13
    dc.b "cid:132", 13
    dc.b "ecl:blu iyr:2016", 13
    dc.b "eyr:2027 byr:1940 hcl:#341e13 hgt:166cm pid:613386501", 13
    dc.b "", 13
    dc.b "pid:360563823 eyr:2028 byr:1990 iyr:2016", 13
    dc.b "ecl:blu cid:287 hgt:162cm hcl:#888785", 13
    dc.b "", 13
    dc.b "hgt:161cm", 13
    dc.b "byr:2002", 13
    dc.b "hcl:#623a2f pid:535361632", 13
    dc.b "ecl:gry eyr:2021 iyr:2013", 13
    dc.b "", 13
    dc.b "hgt:67in", 13
    dc.b "byr:1967", 13
    dc.b "cid:333 hcl:#cfa07d", 13
    dc.b "iyr:2012 eyr:2024 ecl:hzl pid:538161833", 13
    dc.b "", 13
    dc.b "ecl:#2bc145 eyr:1963 iyr:2030", 13
    dc.b "cid:241 hcl:2fc384 hgt:156in pid:2899917140", 13
    dc.b "byr:2005", 13
    dc.b "", 13
    dc.b "eyr:2021 pid:021590229 ecl:gry", 13
    dc.b "hgt:164cm iyr:2013 hcl:#efcc98 byr:1985", 13
    dc.b "", 13
    dc.b "ecl:hzl byr:1943", 13
    dc.b "cid:279 pid:979130395", 13
    dc.b "iyr:2011", 13
    dc.b "hgt:165cm", 13
    dc.b "eyr:2021", 13
    dc.b "hcl:#f331b3", 13
    dc.b "", 13
    dc.b "hgt:161cm", 13
    dc.b "hcl:#888785 byr:1981 pid:835477382 eyr:2025 iyr:2012", 13
    dc.b "cid:348", 13
    dc.b "ecl:blu", 13
    dc.b "", 13
    dc.b "hgt:159cm hcl:b4ce6a cid:319 eyr:2035 iyr:1965 ecl:oth", 13
    dc.b "byr:2010 pid:158cm", 13
    dc.b "", 13
    dc.b "iyr:2020", 13
    dc.b "eyr:2026 ecl:grn hcl:#a97842 pid:126915503", 13
    dc.b "hgt:178cm byr:1986", 13
    dc.b "", 13
    dc.b "hgt:184cm ecl:hzl", 13
    dc.b "cid:67 iyr:2020 eyr:2026 pid:168775568 byr:1944 hcl:#a97842", 13
    dc.b "", 13
    dc.b "hcl:#fffffd iyr:2016 pid:379463363", 13
    dc.b "ecl:oth", 13
    dc.b "hgt:179cm byr:1988", 13
    dc.b "eyr:2028", 13
    dc.b "", 13
    dc.b "hcl:#cfa07d ecl:amb eyr:2030 pid:320360020", 13
    dc.b "iyr:2016 hgt:172cm byr:1961", 13
    dc.b "", 13
    dc.b "cid:221 hcl:#cfa07d byr:1946 eyr:2024 ecl:oth pid:066950409 hgt:173cm", 13
    dc.b "iyr:2020", 13
    dc.b "", 13
    dc.b "hcl:#602927 eyr:2028 ecl:gry iyr:2019 pid:583204134 byr:1966 hgt:178cm", 13
    dc.b "", 13
    dc.b "byr:1930", 13
    dc.b "iyr:2020 ecl:hzl", 13
    dc.b "hcl:#ceb3a1 pid:285751767 cid:287 eyr:2023 hgt:192cm", 13
    dc.b "", 13
    dc.b "eyr:2024", 13
    dc.b "ecl:hzl cid:87 iyr:2015", 13
    dc.b "hgt:152cm hcl:#18171d pid:959574669", 13
    dc.b "byr:1990", 13
    dc.b "", 13
    dc.b "pid:45938863", 13
    dc.b "hcl:49c7ce cid:349 hgt:181cm", 13
    dc.b "eyr:2023 ecl:grn iyr:2015 byr:1948", 13
    dc.b "", 13
    dc.b "hcl:#866857 iyr:2012 ecl:amb cid:132 byr:1955 hgt:162cm pid:597748286 eyr:2023", 13
    dc.b "", 13
    dc.b "pid:293364535 byr:2024", 13
    dc.b "hgt:177cm eyr:2039", 13
    dc.b "iyr:2020 hcl:#dae928 ecl:hzl", 13
    dc.b "", 13
    dc.b "pid:212659709 iyr:2018", 13
    dc.b "hgt:188cm", 13
    dc.b "hcl:#efcc98 byr:1974 eyr:2029 ecl:oth cid:244", 13
    dc.b "", 13
    dc.b "cid:140", 13
    dc.b "ecl:amb", 13
    dc.b "eyr:2022 hgt:181cm hcl:#efcc98", 13
    dc.b "byr:1943", 13
    dc.b "iyr:2016", 13
    dc.b "", 13
    dc.b "cid:71 hgt:151cm pid:5063555219 eyr:2023 ecl:hzl", 13
    dc.b "byr:2019", 13
    dc.b "hcl:#7d3b0c iyr:2023", 13
    dc.b "", 13
    dc.b "hgt:157in pid:#298b06 iyr:2030 ecl:#66a631 eyr:2035 hcl:z byr:2019", 13
    dc.b "", 13
    dc.b "hgt:190cm iyr:1943", 13
    dc.b "pid:644021656 hcl:#6b621c", 13
    dc.b "ecl:oth eyr:2021 byr:1923", 13
    dc.b "", 13
    dc.b "ecl:hzl iyr:2012 eyr:2023 pid:881271720 hcl:#ceb3a1 hgt:172cm", 13
    dc.b "byr:1957", 13
    dc.b "", 13
    dc.b "iyr:2017 hcl:#888785", 13
    dc.b "ecl:amb hgt:170cm byr:1967 pid:198856675 eyr:2027", 13
    dc.b "", 13
    dc.b "eyr:2026", 13
    dc.b "ecl:gry", 13
    dc.b "pid:834980363 hcl:#733820 byr:1930", 13
    dc.b "hgt:175cm iyr:2018", 13
    dc.b "cid:214", 13
    dc.b "", 13
    dc.b "hcl:#efcc98 eyr:2029 iyr:2010 pid:980087545", 13
    dc.b "ecl:brn hgt:157cm", 13
    dc.b "", 13
    dc.b "pid:57513658 iyr:2011 byr:1993 ecl:brn eyr:2027 hcl:#6b5442 hgt:165cm", 13
    dc.b "", 13
    dc.b "ecl:hzl", 13
    dc.b "eyr:2025", 13
    dc.b "hcl:#733820", 13
    dc.b "hgt:169cm iyr:2018 cid:328 byr:1999 pid:694719489", 13
    dc.b "", 13
    dc.b "eyr:2023", 13
    dc.b "cid:125 byr:1925", 13
    dc.b "hgt:185cm pid:806769540 iyr:2013 ecl:hzl", 13
    dc.b "hcl:#866857", 13
    dc.b "", 13
    dc.b "iyr:2010 cid:225", 13
    dc.b "ecl:hzl eyr:2027 pid:615545523", 13
    dc.b "hcl:#733820", 13
    dc.b "byr:1994", 13
    dc.b "hgt:166cm", 13
    dc.b "", 13
    dc.b "byr:1941 ecl:gry iyr:2019 eyr:2026 hgt:73cm hcl:#602927", 13
    dc.b "pid:352996721", 13
    dc.b "", 13
    dc.b "pid:140250433", 13
    dc.b "eyr:2030 ecl:grn", 13
    dc.b "hcl:#fffffd iyr:2011 byr:1937 hgt:185cm", 13
    dc.b "", 13
    dc.b "ecl:gry byr:2002 iyr:2017 hcl:#b6652a cid:261 pid:178cm eyr:2022 hgt:166cm", 13
    dc.b "", 13
    dc.b "ecl:grn iyr:2010 eyr:2022 byr:1924", 13
    dc.b "pid:214641920 hcl:#ceb3a1", 13
    dc.b "hgt:155cm", 13
    dc.b "", 13
    dc.b "hcl:z pid:150cm ecl:utc iyr:1981", 13
    dc.b "eyr:2034", 13
    dc.b "hgt:156in cid:260 byr:2027", 13
    dc.b "", 13
    dc.b "byr:1987 hgt:66in", 13
    dc.b "eyr:2021 pid:876757018 iyr:2015 hcl:d596e4 ecl:hzl", 13
    dc.b "", 13
    dc.b "cid:116 ecl:oth hgt:180cm", 13
    dc.b "iyr:2020 byr:1942 hcl:#2fc31f", 13
    dc.b "eyr:2027", 13
    dc.b "pid:253569416", 13
    dc.b "", 13
    dc.b "pid:509387921", 13
    dc.b "eyr:2022", 13
    dc.b "hcl:#888785 ecl:oth hgt:193cm", 13
    dc.b "iyr:2012 cid:97", 13
    dc.b "byr:1975", 13
    dc.b "", 13
    dc.b "hcl:#18171d hgt:190cm pid:062827417 byr:1939", 13
    dc.b "iyr:2019 eyr:2022", 13
    dc.b "ecl:hzl", 13
    dc.b "", 13
    dc.b "iyr:2025", 13
    dc.b "byr:2028", 13
    dc.b "hgt:165in eyr:2027 pid:6259332452", 13
    dc.b "hcl:#478251", 13
    dc.b "", 13
    dc.b "iyr:2018 eyr:2026 pid:523863237", 13
    dc.b "hgt:187cm", 13
    dc.b "ecl:oth", 13
    dc.b "byr:1944", 13
    dc.b "hcl:#a97842", 13
    dc.b "", 13
    dc.b "hgt:181cm hcl:#733820 pid:923996316", 13
    dc.b "cid:110", 13
    dc.b "iyr:2011 byr:1949 ecl:blu eyr:2023", 13
    dc.b "", 13
    dc.b "pid:304792392 hcl:487823 eyr:2020", 13
    dc.b "hgt:70cm byr:2024", 13
    dc.b "iyr:1953", 13
    dc.b "ecl:blu", 13
    dc.b "", 13
    dc.b "pid:142200694", 13
    dc.b "ecl:oth hcl:#888785 eyr:2028", 13
    dc.b "hgt:152cm byr:1954 iyr:2018", 13
    dc.b "", 13
    dc.b "ecl:utc", 13
    dc.b "iyr:2015 byr:1932 hcl:#623a2f", 13
    dc.b "eyr:2027 hgt:183cm pid:036300444", 13
    dc.b "", 13
    dc.b "iyr:2014 ecl:hzl byr:1935 hgt:190cm hcl:#efcc98 pid:945893288", 13
    dc.b "eyr:2025", 13
    dc.b "", 13
    dc.b "hcl:#efcc98 pid:252639104 hgt:188cm", 13
    dc.b "byr:1998 iyr:2019 ecl:grn", 13
    dc.b "eyr:2023", 13
    dc.b "", 13
    dc.b "hcl:58aa4a byr:1930 hgt:193cm", 13
    dc.b "iyr:1998 cid:196 ecl:brn", 13
    dc.b "eyr:2032", 13
    dc.b "", 13
    dc.b "iyr:2015 ecl:hzl", 13
    dc.b "hgt:193cm pid:653794674 eyr:2024", 13
    dc.b "hcl:#fffffd byr:1921", 13
    dc.b "", 13
    dc.b "pid:980680460 byr:1962 ecl:blu", 13
    dc.b "iyr:2013", 13
    dc.b "hcl:#72cace", 13
    dc.b "eyr:2030", 13
    dc.b "hgt:180cm", 13
    dc.b "", 13
    dc.b "eyr:2025", 13
    dc.b "hgt:182cm hcl:#ceb3a1 iyr:2010 byr:1945 cid:314 pid:597769706 ecl:amb", 13
    dc.b "", 13
    dc.b "pid:761757504", 13
    dc.b "hcl:#888785 hgt:161cm iyr:2015", 13
    dc.b "byr:1939 eyr:2025", 13
    dc.b "cid:326 ecl:blu", 13
    dc.b "", 13
    dc.b "ecl:gry", 13
    dc.b "hgt:163cm byr:1981", 13
    dc.b "pid:330818500 iyr:2017 eyr:2024", 13
    dc.b "cid:71 hcl:#888785", 13
    dc.b "", 13
    dc.b "pid:190cm cid:267 iyr:2015 ecl:brn", 13
    dc.b "hcl:869252", 13
    dc.b "byr:1935 hgt:142 eyr:2033", 13
    dc.b "", 13
    dc.b "cid:239", 13
    dc.b "eyr:2038 ecl:lzr hcl:z iyr:1987 pid:4632768239", 13
    dc.b "hgt:162in", 13
    dc.b "", 13
    dc.b "pid:158038227 ecl:brn byr:1995 eyr:2028 hcl:#efcc98", 13
    dc.b "cid:252 iyr:2021", 13
    dc.b "hgt:184cm", 13
    dc.b "", 13
    dc.b "eyr:2027", 13
    dc.b "cid:124 ecl:amb hgt:165cm byr:1949", 13
    dc.b "pid:727126101 iyr:2010 hcl:#602927", 13
    dc.b "", 13
    dc.b "ecl:grn", 13
    dc.b "byr:1966 pid:184245393 hgt:164cm", 13
    dc.b "eyr:2022", 13
    dc.b "iyr:2014 hcl:#866857", 13
    dc.b "", 13
    dc.b "cid:62 hgt:180cm eyr:2027 hcl:#18171d", 13
    dc.b "iyr:2017 ecl:blu byr:1942 pid:930210027", 13
    dc.b "", 13
    dc.b "ecl:grn hgt:171cm iyr:2017 hcl:#fffffd eyr:2029 byr:1946 pid:863414762", 13
    dc.b "cid:95", 13
    dc.b "", 13
    dc.b "eyr:2025 ecl:grn iyr:2019 cid:226 hcl:#b6652a", 13
    dc.b "byr:1932 pid:715708549", 13
    dc.b "hgt:156cm", 13
    dc.b "", 13
    dc.b "pid:505158338 iyr:2019 byr:1981 hgt:193cm", 13
    dc.b "hcl:#696a5c cid:57 ecl:hzl eyr:2023", 13
    dc.b "", 13
    dc.b "byr:1987", 13
    dc.b "hgt:155cm cid:99 ecl:grn iyr:2010", 13
    dc.b "hcl:#c0946f eyr:2023", 13
    dc.b "pid:431067921", 13
    dc.b "", 13
    dc.b "hgt:190in", 13
    dc.b "hcl:z eyr:2029 pid:74228790", 13
    dc.b "iyr:2016 byr:2018 ecl:brn", 13
    dc.b "", 13
    dc.b "eyr:2022", 13
    dc.b "ecl:xry hgt:154cm pid:62205162", 13
    dc.b "iyr:2014 byr:1936", 13
    dc.b "cid:61", 13
    dc.b "", 13
    dc.b "ecl:amb eyr:2026", 13
    dc.b "byr:1966 cid:95 hcl:#733820 pid:957767251 iyr:2013 hgt:157cm", 13
    dc.b "", 13
    dc.b "byr:1969", 13
    dc.b "hgt:156cm iyr:2013 ecl:blu hcl:#a97842", 13
    dc.b "cid:183", 13
    dc.b "pid:960672229 eyr:2020", 13
    dc.b "", 13
    dc.b "iyr:2013", 13
    dc.b "cid:243 eyr:2028 hgt:192cm hcl:#efcc98", 13
    dc.b "ecl:grn pid:222407433 byr:1978", 13
    dc.b "", 13
    dc.b "iyr:2014 byr:1935", 13
    dc.b "eyr:2021 cid:235 pid:#1b34e1", 13
    dc.b "hcl:#89313f hgt:164cm ecl:blu", 13
    dc.b "", 13
    dc.b "ecl:hzl iyr:2016 cid:327", 13
    dc.b "byr:1923 pid:695935353 hgt:184cm", 13
    dc.b "hcl:#a97842", 13
    dc.b "eyr:2028", 13
    dc.b "", 13
    dc.b "pid:6010745668", 13
    dc.b "byr:1934 ecl:oth eyr:2020 hgt:164cm", 13
    dc.b "hcl:#733820", 13
    dc.b "iyr:2016", 13
    dc.b "", 13
    dc.b "ecl:blu pid:071991002 eyr:2021 byr:1978 cid:321", 13
    dc.b "hcl:#efcc98", 13
    dc.b "iyr:2013 hgt:68in", 13
    dc.b "", 13
    dc.b "ecl:grn iyr:2015 pid:137792524 cid:156", 13
    dc.b "hcl:#efcc98", 13
    dc.b "eyr:2029 byr:1955", 13
    dc.b "hgt:165cm", 13
    dc.b "", 13
    dc.b "byr:1949", 13
    dc.b "hgt:176cm pid:531868428", 13
    dc.b "hcl:#cfa07d ecl:brn iyr:2014 eyr:2024", 13
    dc.b "", 13
    dc.b "iyr:1955 cid:108 pid:712137140 byr:2019 eyr:2040 hgt:184cm hcl:220cfe ecl:#551592", 13
    dc.b "", 13
    dc.b "iyr:2016 eyr:2030", 13
    dc.b "hgt:177cm cid:137 ecl:brn", 13
    dc.b "hcl:#efcc98 pid:712202745 byr:1938", 13
    dc.b "", 13
    dc.b "pid:357180007 iyr:2010 ecl:grn", 13
    dc.b "byr:1991", 13
    dc.b "hcl:#341e13", 13
    dc.b "eyr:2020 hgt:159cm", 13
    dc.b "", 13
    dc.b "eyr:2023 ecl:grn", 13
    dc.b "hcl:#733820 iyr:2020 byr:1927 hgt:151cm", 13
    dc.b "pid:165936826", 13
    dc.b "", 13
    dc.b "ecl:gry", 13
    dc.b "pid:794227261 iyr:2014 eyr:2030", 13
    dc.b "hcl:#18171d", 13
    dc.b "byr:1994", 13
    dc.b "hgt:162cm", 13
    dc.b "", 13
    dc.b "iyr:2017 eyr:2024", 13
    dc.b "hcl:#7d3b0c cid:279", 13
    dc.b "ecl:gry byr:1981 hgt:176cm pid:973822115", 13
    dc.b "", 13
    dc.b "eyr:2029", 13
    dc.b "hgt:152cm hcl:#fffffd ecl:amb byr:1946 iyr:2013", 13
    dc.b "cid:62 pid:005240023", 13
    dc.b "", 13
    dc.b "iyr:2010", 13
    dc.b "ecl:amb hcl:#341e13 hgt:184cm", 13
    dc.b "eyr:2027", 13
    dc.b "pid:976217816 byr:1950", 13
    dc.b "", 13
    dc.b "ecl:grn hgt:178cm cid:192 hcl:#602927 pid:684333017 eyr:2022", 13
    dc.b "iyr:2011 byr:1987", 13
    dc.b "", 13
    dc.b "pid:306960973 ecl:hzl hgt:168cm", 13
    dc.b "byr:1954 iyr:2015 eyr:2029 hcl:#602927", 13
    dc.b "", 13
    dc.b "hcl:#18171d", 13
    dc.b "byr:1973 ecl:hzl hgt:174cm pid:922891164", 13
    dc.b "iyr:2013", 13
    dc.b "eyr:2023", 13
    dc.b "", 13
    dc.b "byr:1998 hgt:189cm pid:472066200 ecl:gry iyr:2012 eyr:2021 hcl:#c0946f cid:299", 13
    dc.b "", 13
    dc.b "iyr:2014", 13
    dc.b "eyr:2028 byr:1922 pid:594856217 hgt:158cm", 13
    dc.b "ecl:oth", 13
    dc.b "hcl:#623a2f", 13
    dc.b "", 13
    dc.b "pid:215206381 byr:1928", 13
    dc.b "hgt:163cm", 13
    dc.b "hcl:#b6652a ecl:oth iyr:2011", 13
    dc.b "", 13
    dc.b "cid:145 iyr:2013", 13
    dc.b "ecl:#38a290", 13
    dc.b "eyr:2034", 13
    dc.b "hcl:#602927 hgt:186cm pid:517498756", 13
    dc.b "byr:1945", 13
    dc.b "", 13
    dc.b "hcl:#5637d2 eyr:2030 byr:1955", 13
    dc.b "hgt:187cm", 13
    dc.b "pid:862655087 iyr:2014 ecl:grn", 13
    dc.b "", 13
    dc.b "hcl:#7d3b0c hgt:176cm iyr:2019", 13
    dc.b "eyr:2029 byr:1980 ecl:hzl", 13
    dc.b "cid:346 pid:703908707", 13
    dc.b "", 13
    dc.b "hgt:185cm", 13
    dc.b "iyr:2017", 13
    dc.b "cid:120 eyr:2020 hcl:#733820 ecl:blu pid:458522542 byr:1966", 13
    dc.b "", 13
    dc.b "pid:#725759", 13
    dc.b "hcl:#602927 iyr:2013 byr:2003 eyr:2023 cid:100", 13,0
INPUTEND: