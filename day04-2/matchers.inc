; *************************************************************
; Advent Of Code 2020 Day 04 - Part 2
; Copyright (c) 2020 Ross Bamford (roscopeco@gmail.com)
;
; Matcher NFAs for Day 04 part 2
;
; Ideas for improvement:
;   * A lot of duplication - many of the states could be combined,
;     if instead of starting at state zero a start state could be specified
;
;   * CAPOFF is unnecessary in these, as currently used (as we go directly
;     to STOP after disabling capture, which implicitly does that anyway...
; *************************************************************
    include "matchy.inc"

; ****************************************************************************
; Equivalent to /.*?byr:(\d{4})/
;
BYR_NFA:
    dc.w  17        ; 17 states

; state 0 - choice: 'b', or non-greedy any match
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  1         ; Try state 1 first
    dc.w  15        ; Else try state 15
    dc.w  0         ; Data (not used)

; state 1 - match 'b'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  2         ; Next state is 2
    dc.w  0         ; T1 (not used)
    dc.b  'b'       ; Data
    dc.b  0         ; Padding
    
; state 2 - match 'y'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  3         ; Next state is 3
    dc.w  0         ; T1 (not used)
    dc.b  'y'       ; Data
    dc.b  0         ; Padding

; state 3 - match 'r'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  4         ; Next state is 4
    dc.w  0         ; T1 (not used)
    dc.b  'r'       ; Data
    dc.b  0         ; Padding

; state 4 - match ':'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  5         ; Next state is 5
    dc.w  0         ; T1 (not used)
    dc.b  ':'       ; Data
    dc.b  0         ; Padding

; state 5 - start capturing
    dc.w  OP_CAPON  ; Opcode - capture on
    dc.w  6         ; Next state is 6
    dc.w  0         ; T1 (not used)
    dc.w  0         ; Data (not used)

; state 6 - match any digit (first)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  7         ; Next state is 7
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 7 - match any digit (second)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  8         ; Next state is 8
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 8 - match any digit (third)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  9         ; Next state is 9
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 9 - match any digit (fourth), using a character class
;           This is here as an example (and test) of character
;           classes. RANGE would be more efficient for this case.
    dc.w  OP_CLASS  ; Opcode - match class
    dc.w  14        ; Next state is 14
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Flags (reserved)
    dc.b  '0'       ; Flags (reserved)

; state 10 - Bitmap 1 for OP_CLASS (state 9)
    dc.l  %00000000000000000000000000000000    ; 31-0
    dc.l  %00000011111111110000000000000000    ; 63-32
; state 11 - Bitmap 2 for OP_CLASS (state 9)
    dc.l  %00000000000000000000000000000000    ; 95-64
    dc.l  %00000000000000000000000000000000    ; 127-96
; state 12 - Bitmap 3 for OP_CLASS (state 9)
    dc.l  %00000000000000000000000000000000    ; 159-128
    dc.l  %00000000000000000000000000000000    ; 191-160
; state 13 - Bitmap 4 for OP_CLASS (state 9)
    dc.l  %00000000000000000000000000000000    ; 223-192
    dc.l  %00000000000000000000000000000000    ; 255-224

; state 14 - stop capturing
    dc.w  OP_CAPOFF ; Opcode - capture off
    dc.w  16        ; Next state is 16
    dc.w  0         ; T1 (not used)
    dc.w  0         ; Data (not used)

; state 15 - any (alternate state from 0)
    dc.w  OP_ANY    ; Opcode - any
    dc.w  0         ; Mext state is 0
    dc.w  0         ; T1 (not used)  
    dc.w  0         ; Data (not used)

; state 16 - stop (success)
    dc.w  OP_STOP   ; Opcode - stop
                    ; The rest is not used for a stop... 
    
; ****************************************************************************
; Equivalent to /.*?iyr:(\d{4})/
;
IYR_NFA:
    dc.w  13        ; 13 states

; state 0 - choice: 'i', or non-greedy any match
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  1         ; Try state 1 first
    dc.w  11        ; Else try state 11
    dc.w  0         ; Data (not used)

; state 1 - match 'i'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  2         ; Next state is 2
    dc.w  0         ; T1 (not used)
    dc.b  'i'       ; Data
    dc.b  0         ; Padding
    
; state 2 - match 'y'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  3         ; Next state is 3
    dc.w  0         ; T1 (not used)
    dc.b  'y'       ; Data
    dc.b  0         ; Padding

; state 3 - match 'r'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  4         ; Next state is 4
    dc.w  0         ; T1 (not used)
    dc.b  'r'       ; Data
    dc.b  0         ; Padding

; state 4 - match ':'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  5         ; Next state is 5
    dc.w  0         ; T1 (not used)
    dc.b  ':'       ; Data
    dc.b  0         ; Padding

; state 5 - start capturing
    dc.w  OP_CAPON  ; Opcode - capture on
    dc.w  6         ; Next state is 6
    dc.w  0         ; T1 (not used)
    dc.w  0         ; Data (not used)

; state 6 - match any digit (first)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  7         ; Next state is 7
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 7 - match any digit (second)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  8         ; Next state is 8
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 8 - match any digit (third)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  9         ; Next state is 9
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 9 - match any digit (fourth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  10        ; Next state is 10
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 10 - stop capturing
    dc.w  OP_CAPOFF ; Opcode - capture off
    dc.w  12        ; Next state is 12
    dc.w  0         ; T1 (not used)
    dc.w  0         ; Data (not used)

; state 11 - any (alternate state from 0)
    dc.w  OP_ANY    ; Opcode - any
    dc.w  0         ; Mext state is 0
    dc.w  0         ; T1 (not used)  
    dc.w  0         ; Data (not used)

; state 12 - stop (success)
    dc.w  OP_STOP   ; Opcode - stop
                    ; The rest is not used for a stop... 

; ****************************************************************************
; Equivalent to /.*?eyr:(\d{4})/
;
EYR_NFA:
    dc.w  13        ; 13 states

; state 0 - choice: 'e', or non-greedy any match
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  1         ; Try state 1 first
    dc.w  11        ; Else try state 11
    dc.w  0         ; Data (not used)

; state 1 - match 'e'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  2         ; Next state is 2
    dc.w  0         ; T1 (not used)
    dc.b  'e'       ; Data
    dc.b  0         ; Padding
    
; state 2 - match 'y'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  3         ; Next state is 3
    dc.w  0         ; T1 (not used)
    dc.b  'y'       ; Data
    dc.b  0         ; Padding

; state 3 - match 'r'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  4         ; Next state is 4
    dc.w  0         ; T1 (not used)
    dc.b  'r'       ; Data
    dc.b  0         ; Padding

; state 4 - match ':'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  5         ; Next state is 5
    dc.w  0         ; T1 (not used)
    dc.b  ':'       ; Data
    dc.b  0         ; Padding

; state 5 - start capturing
    dc.w  OP_CAPON  ; Opcode - capture on
    dc.w  6         ; Next state is 6
    dc.w  0         ; T1 (not used)
    dc.w  0         ; Data (not used)

; state 6 - match any digit (first)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  7         ; Next state is 7
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 7 - match any digit (second)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  8         ; Next state is 8
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 8 - match any digit (third)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  9         ; Next state is 9
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 9 - match any digit (fourth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  10        ; Next state is 10
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 10 - stop capturing
    dc.w  OP_CAPOFF ; Opcode - capture off
    dc.w  12        ; Next state is 12
    dc.w  0         ; T1 (not used)
    dc.w  0         ; Data (not used)

; state 11 - any (alternate state from 0)
    dc.w  OP_ANY    ; Opcode - any
    dc.w  0         ; Mext state is 0
    dc.w  0         ; T1 (not used)  
    dc.w  0         ; Data (not used)

; state 12 - stop (success)
    dc.w  OP_STOP   ; Opcode - stop
                    ; The rest is not used for a stop... 

; ****************************************************************************
; Equivalent to /.*?hgt:(\d\d\d?(?:cm|in))/
;
HGT_NFA:
    dc.w  18        ; 18 states
    
; state 0 - choice: 'h', or non-greedy any match
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  1         ; Try state 1 first
    dc.w  16        ; Else try state 5
    dc.w  0         ; Data (not used)

; state 1 - match 'h'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  2         ; Next state is 2
    dc.w  0         ; T1 (not used)
    dc.b  'h'       ; Data
    dc.b  0         ; Padding
    
; state 2 - match 'g'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  3         ; Next state is 3
    dc.w  0         ; T1 (not used)
    dc.b  'g'       ; Data
    dc.b  0         ; Padding

; state 3 - match 't'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  4         ; Next state is 4
    dc.w  0         ; T1 (not used)
    dc.b  't'       ; Data
    dc.b  0         ; Padding

; state 4 - match ':'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  5         ; Next state is 5
    dc.w  0         ; T1 (not used)
    dc.b  ':'       ; Data
    dc.b  0         ; Padding

; state 5 - start capture
    dc.w  OP_CAPON  ; Opcode - Start capture
    dc.w  6         ; Next state is 6
    dc.w  0         ; T1 (not used)
    dc.w  0         ; Data (not used)

; state 6 - match first digit 
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  7         ; Next state is 7
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 7 - match second digit
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  8         ; Next state is 8
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 8 - choice: optional third digit or further choices
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  9         ; Try state 9 first
    dc.w  10        ; Else directly to state 10
    dc.w  0         ; Data (not used)

; state 9 - match second digit
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  10        ; Next state is 10
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 10 - choice: 'cm' branch or 'in' branch
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  11        ; Try state 11 first
    dc.w  13        ; Else try state 13
    dc.w  0         ; Data (not used)

; state 11 - match 'c'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  12        ; Next state is 12
    dc.w  0         ; T1 (not used)
    dc.b  'c'       ; Data
    dc.b  0         ; Padding

; state 12 - match 'm'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  15        ; Next state is 15 (STOP CAPTURE & STOP)
    dc.w  0         ; T1 (not used)
    dc.b  'm'       ; Data
    dc.b  0         ; Padding

; state 13 - match 'i'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  14        ; Next state is 14
    dc.w  0         ; T1 (not used)
    dc.b  'i'       ; Data
    dc.b  0         ; Padding

; state 14 - match 'n'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  15        ; Next state is 15 (STOP CAPTURE AND STOP)
    dc.w  0         ; T1 (not used)
    dc.b  'n'       ; Data
    dc.b  0         ; Padding

; state 15 - stop capture
    dc.w  OP_CAPOFF ; Opcode - stop capture
    dc.w  17        ; Mext state is 17
    dc.w  0
    dc.w  0

; state 16 - any (alternate state from 0)
    dc.w  OP_ANY    ; Opcode - any
    dc.w  0         ; Mext state is 0
    dc.w  0
    dc.w  0

; state 17 - stop (success)
    dc.w  OP_STOP   ; Opcode - stop
                    ; The rest is not used for a stop... 

; ****************************************************************************
; Equivalent to /.*?hcl:[0-9a-f]{4}/
;
HCL_NFA:
    dc.w  26        ; 26 states
    
; state 0 - choice: 'h', or non-greedy any match
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  1         ; Try state 1 first
    dc.w  24        ; Else try state 24
    dc.w  0         ; Data (not used)

; state 1 - match 'h'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  2         ; Next state is 2
    dc.w  0         ; T1 (not used)
    dc.b  'h'       ; Data
    dc.b  0         ; Padding
    
; state 2 - match 'c'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  3         ; Next state is 3
    dc.w  0         ; T1 (not used)
    dc.b  'c'       ; Data
    dc.b  0         ; Padding

; state 3 - match 'l'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  4         ; Next state is 4
    dc.w  0         ; T1 (not used)
    dc.b  'l'       ; Data
    dc.b  0         ; Padding

; state 4 - match ':'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  5         ; Next state is 5
    dc.w  0         ; T1 (not used)
    dc.b  ':'       ; Data
    dc.b  0         ; Padding

; state 5 - match '#'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  6         ; Next state is 6
    dc.w  0         ; T1 (not used)
    dc.b  '#'       ; Data
    dc.b  0         ; Padding

; *** First hex digit match
; state 6 - choice '0-9' or 'a-f'
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  7         ; Try state 7 first
    dc.w  8         ; Else try state 8
    dc.w  0         ; Data (not used)

; state 7 - match any digit (first)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  9         ; Next state is 9
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 8 - match any 'a' to 'f' (first)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  9         ; Next state is 9
    dc.w  0         ; T1 (not used)
    dc.b  'a'       ; Lower bound is 'a'
    dc.b  'f'       ; Upper bound is 'f'

; *** Second hex digit match
; state 9 - choice '0-9' or 'a-f'
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  10        ; Try state 10 first
    dc.w  11        ; Else try state 11
    dc.w  0         ; Data (not used)

; state 10 - match any digit (second)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  12        ; Next state is 12
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 11 - match any 'a' to 'f' (second)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  12        ; Next state is 12
    dc.w  0         ; T1 (not used)
    dc.b  'a'       ; Lower bound is 'a'
    dc.b  'f'       ; Upper bound is 'f'

; *** Third hex digit match
; state 12 - choice '0-9' or 'a-f'
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  13        ; Try state 13 first
    dc.w  14        ; Else try state 14
    dc.w  0         ; Data (not used)

; state 13 - match any digit (third)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  15        ; Next state is 15
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 14 - match any 'a' to 'f' (third)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  15        ; Next state is 15
    dc.w  0         ; T1 (not used)
    dc.b  'a'       ; Lower bound is 'a'
    dc.b  'f'       ; Upper bound is 'f'

; *** Fourth hex digit match
; state 15 - choice '0-9' or 'a-f'
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  16        ; Try state 16 first
    dc.w  17        ; Else try state 17
    dc.w  0         ; Data (not used)

; state 16 - match any digit (first)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  18        ; Next state is 18
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 17 - match any 'a' to 'f' (first)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  18        ; Next state is 18
    dc.w  0         ; T1 (not used)
    dc.b  'a'       ; Lower bound is 'a'
    dc.b  'f'       ; Upper bound is 'f'

; *** Fifth hex digit match
; state 18 - choice '0-9' or 'a-f'
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  19        ; Try state 19 first
    dc.w  20        ; Else try state 20
    dc.w  0         ; Data (not used)

; state 19 - match any digit (fifth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  21        ; Next state is 21
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 20 - match any 'a' to 'f' (fifth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  21        ; Next state is 21
    dc.w  0         ; T1 (not used)
    dc.b  'a'       ; Lower bound is 'a'
    dc.b  'f'       ; Upper bound is 'f'

; *** Sixth hex digit match
; state 21 - choice '0-9' or 'a-f'
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  22        ; Try state 22 first
    dc.w  23        ; Else try state 23
    dc.w  0         ; Data (not used)

; state 22 - match any digit (sixth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  25        ; Next state is 25
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 23 - match any 'a' to 'f' (sixth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  25        ; Next state is 25
    dc.w  0         ; T1 (not used)
    dc.b  'a'       ; Lower bound is 'a'
    dc.b  'f'       ; Upper bound is 'f'

; state 24 - any (alternate state from 0)
    dc.w  OP_ANY    ; Opcode - any
    dc.w  0         ; Mext state is 0
    dc.w  0
    dc.w  0

; state 25 - stop (success)
    dc.w  OP_STOP   ; Opcode - stop
                    ; The rest is not used for a stop... 

; ****************************************************************************
; Equivalent to /.*?ecl:(?:amb|hzl|oth|gr(?:y|n)|b(?:lu|rn))/
;
ECL_NFA:
    dc.w  31        ; 31 states
    
; state 0 - choice: 'e', or non-greedy any match
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  1         ; Try state 1 first
    dc.w  29        ; Else try state 29
    dc.w  0         ; Data (not used)

; state 1 - match 'e'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  2         ; Next state is 2
    dc.w  0         ; T1 (not used)
    dc.b  'e'       ; Data
    dc.b  0         ; Padding
    
; state 2 - match 'c'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  3         ; Next state is 3
    dc.w  0         ; T1 (not used)
    dc.b  'c'       ; Data
    dc.b  0         ; Padding

; state 3 - match 'l'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  4         ; Next state is 4
    dc.w  0         ; T1 (not used)
    dc.b  'l'       ; Data
    dc.b  0         ; Padding

; state 4 - match ':'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  5         ; Next state is 5
    dc.w  0         ; T1 (not used)
    dc.b  ':'       ; Data
    dc.b  0         ; Padding

; *** Begin nested choices to match valid colours
; state 5 - choice: 'a' or further choices
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  6         ; Try state 6 first ('amb' branch)
    dc.w  9         ; Else try state 9 (other choices)
    dc.w  0         ; Data (Not used)

; state 6 - match 'a'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  7         ; Next state is 7
    dc.w  0         ; T1 (not used)
    dc.b  'a'       ; Data
    dc.b  0         ; Padding

; state 7 - match 'm'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  8         ; Next state is 8
    dc.w  0         ; T1 (not used)
    dc.b  'm'       ; Data
    dc.b  0         ; Padding

; state 8 - match 'b'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  30        ; Next state is 30 (STOP)
    dc.w  0         ; T1 (not used)
    dc.b  'b'       ; Data
    dc.b  0         ; Padding

; state 9 - choice: 'h' or further choices
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  10        ; Try state 10 first ('hzl' branch)
    dc.w  13        ; Else try state 13 (other choices)
    dc.w  0         ; Data (Not used)

; state 10 - match 'h'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  11        ; Next state is 11
    dc.w  0         ; T1 (not used)
    dc.b  'h'       ; Data
    dc.b  0         ; Padding

; state 11 - match 'z'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  12        ; Next state is 12
    dc.w  0         ; T1 (not used)
    dc.b  'z'       ; Data
    dc.b  0         ; Padding

; state 12 - match 'l'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  30        ; Next state is 30 (STOP)
    dc.w  0         ; T1 (not used)
    dc.b  'l'       ; Data
    dc.b  0         ; Padding

; state 13 - choice: 'o' or further choices
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  14        ; Try state 14 first ('oth' branch)
    dc.w  17        ; Else try state 17 (other choices)
    dc.w  0         ; Data (Not used)

; state 14 - match 'o'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  15        ; Next state is 15
    dc.w  0         ; T1 (not used)
    dc.b  'o'       ; Data
    dc.b  0         ; Padding

; state 15 - match 't'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  16        ; Next state is 16
    dc.w  0         ; T1 (not used)
    dc.b  't'       ; Data
    dc.b  0         ; Padding

; state 16 - match 'h'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  30        ; Next state is 30 (STOP)
    dc.w  0         ; T1 (not used)
    dc.b  'h'       ; Data
    dc.b  0         ; Padding

; state 17 - choice: 'grX' or 'bXX' branches
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  18        ; Try state 18 first ('grX' branch)
    dc.w  23        ; Else try state 23 ('bXX' branch)
    dc.w  0         ; Data (Not used)

; state 18 - match 'g'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  19        ; Next state is 19
    dc.w  0         ; T1 (not used)
    dc.b  'g'       ; Data
    dc.b  0         ; Padding

; state 19 - match 'r'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  20        ; Next state is 20
    dc.w  0         ; T1 (not used)
    dc.b  'r'       ; Data
    dc.b  0         ; Padding

; state 20 - choice: '<gr>y' or '<gr>n' branches
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  21        ; Try state 21 first ('<gr>y')
    dc.w  22        ; Else try state 22 ('<gr>n')
    dc.w  0         ; Data (Not used)

; state 21 - match 'y'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  30        ; Next state is 30 (STOP)
    dc.w  0         ; T1 (not used)
    dc.b  'y'       ; Data
    dc.b  0         ; Padding

; state 22 - match 'n'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  30        ; Next state is 30 (STOP)
    dc.w  0         ; T1 (not used)
    dc.b  'n'       ; Data
    dc.b  0         ; Padding

; state 23 - match 'b'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  24        ; Next state is 24 
    dc.w  0         ; T1 (not used)
    dc.b  'b'       ; Data
    dc.b  0         ; Padding
    
; state 24 - choice: '<b>lu' or '<b>rn' branches
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  25        ; Try state 25 first ('<b>lu' branch)
    dc.w  27        ; Else try state 27 ('<b>rn' branch)
    dc.w  0         ; Data (Not used)

; state 25 - match 'l'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  26        ; Next state is 26
    dc.w  0         ; T1 (not used)
    dc.b  'l'       ; Data
    dc.b  0         ; Padding
    
; state 26 - match 'u'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  30        ; Next state is 30 (STOP)
    dc.w  0         ; T1 (not used)
    dc.b  'u'       ; Data
    dc.b  0         ; Padding
    
; state 27 - match 'r'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  28        ; Next state is 28
    dc.w  0         ; T1 (not used)
    dc.b  'r'       ; Data
    dc.b  0         ; Padding
    
; state 28 - match 'n'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  30        ; Next state is 30 (STOP)
    dc.w  0         ; T1 (not used)
    dc.b  'n'       ; Data
    dc.b  0         ; Padding

; state 29 - any (alternate state from 0)
    dc.w  OP_ANY    ; Opcode - any
    dc.w  0         ; Next state is 0
    dc.w  0
    dc.w  0

; state 30 - stop (success)
    dc.w  OP_STOP   ; Opcode - stop
                    ; The rest is not used for a stop... 

; ****************************************************************************
PID_NFA:
; Equivalent to /.*?pid:\d{9}\D/
;
    dc.w  17        ; 17 states
    
; state 0 - choice: 'p', or non-greedy any match
    dc.w  OP_CHOICE ; Opcode - choice
    dc.w  1         ; Try state 1 first
    dc.w  15        ; Else try state 15
    dc.w  0         ; Data (not used)

; state 1 - match 'p'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  2         ; Next state is 2
    dc.w  0         ; T1 (not used)
    dc.b  'p'       ; Data
    dc.b  0         ; Padding
    
; state 2 - match 'i'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  3         ; Next state is 3
    dc.w  0         ; T1 (not used)
    dc.b  'i'       ; Data
    dc.b  0         ; Padding

; state 3 - match 'd'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  4         ; Next state is 4
    dc.w  0         ; T1 (not used)
    dc.b  'd'       ; Data
    dc.b  0         ; Padding

; state 4 - match ':'
    dc.w  OP_MATCH  ; Opcode - match
    dc.w  5         ; Next state is 5
    dc.w  0         ; T1 (not used)
    dc.b  ':'       ; Data
    dc.b  0         ; Padding

; state 5 - match any digit (first)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  6         ; Next state is 6
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 6 - match any digit (second)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  7         ; Next state is 7
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 7 - match any digit (third)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  8         ; Next state is 8
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 8 - match any digit (fourth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  9         ; Next state is 9
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 9 - match any digit (fifth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  10        ; Next state is 10
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 10 - match any digit (sixth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  11        ; Next state is 11
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 11 - match any digit (seventh)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  12        ; Next state is 12
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 12 - match any digit (eighth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  13        ; Next state is 13
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 13 - match any digit (ninth)
    dc.w  OP_RANGE  ; Opcode - match range
    dc.w  14        ; Next state is 14
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 14 - match any non-digit
    dc.w  OP_NRANGE ; Opcode - negative match range
    dc.w  16        ; Next state is 16
    dc.w  0         ; T1 (not used)
    dc.b  '0'       ; Lower bound is '0'
    dc.b  '9'       ; Upper bound is '9'

; state 15 - any (alternate state from 0)
    dc.w  OP_ANY    ; Opcode - any
    dc.w  0         ; Mext state is 0
    dc.w  0
    dc.w  0

; state 16 - stop (success)
    dc.w  OP_STOP   ; Opcode - stop
                    ; The rest is not used for a stop... 

