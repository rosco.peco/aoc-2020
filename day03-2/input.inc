INPUT:
    dc.b ".#.#....##.......#..........#..", 13
    dc.b "...#...........##...#..#.......", 13
    dc.b "#.####......##.#...#......#.#..", 13
    dc.b "##.....#.#.#..#.#............#.", 13
    dc.b "##.....#....#.........#...##...", 13
    dc.b "###..#.....#....#..............", 13
    dc.b "..........#..#.#..#.#....#.....", 13
    dc.b "##.....#....#.#...#.##.........", 13
    dc.b "#...#......#....##....#..#.#...", 13
    dc.b ".##.##...#....##..#.#.....#...#", 13
    dc.b ".....#.#..........##.#........#", 13
    dc.b ".##..................#..#..##.#", 13
    dc.b "#.#..........##....#.####......", 13
    dc.b ".#......#.#......#.........#...", 13
    dc.b "#....#..##.##..##........#.#...", 13
    dc.b "##..#.##..#...#..####.#..#.....", 13
    dc.b "###....#.###.##...........##..#", 13
    dc.b ".....#.##.....##.#..#####....##", 13
    dc.b "....#.###....#..##....##...#...", 13
    dc.b "..###.#...##.....#.##..#..#.#..", 13
    dc.b "#...#..#..#.........#..#.......", 13
    dc.b "##..#.#.....#.#.#.......#...#.#", 13
    dc.b "...#...##.#........#...#.......", 13
    dc.b "..#..#.#..#...#...#...........#", 13
    dc.b "........#.....#......#...##....", 13
    dc.b "#........##.##.#.#...#...#.....", 13
    dc.b "####.......#.##.###.#....#.....", 13
    dc.b "...#...........#...#......#...#", 13
    dc.b "##...#...#............#.......#", 13
    dc.b "....#...........##.......#.....", 13
    dc.b "###......#.....#....#...#.#...#", 13
    dc.b ".....##..........#.......#.#...", 13
    dc.b "##.##.##...#......#....#.......", 13
    dc.b "##..#.#..#......#...#..#.......", 13
    dc.b "....#....##.##............####.", 13
    dc.b "..#.###..#.##.###..#.##.......#", 13
    dc.b "#.##..#.#.....#..#.....##......", 13
    dc.b "..##..#.....##.#.##........#...", 13
    dc.b ".#..#.#......#..#............#.", 13
    dc.b ".....#..#.#...#....#.##.#......", 13
    dc.b ".#...##.#..#.#...##...##..##...", 13
    dc.b "###............#.#..#..#...#...", 13
    dc.b "..#..##.####.#.....#.....##.###", 13
    dc.b "#....#.##..##....#..#...#.##.#.", 13
    dc.b ".....#.##.........##...##......", 13
    dc.b ".........####.#....#.#......#.#", 13
    dc.b ".........#.#..#...#.#..#.#....#", 13
    dc.b ".#.....#..##.##..##....#.......", 13
    dc.b "..........##......#.##.###....#", 13
    dc.b ".##...###..##.#...#........##..", 13
    dc.b "..............#.#....#.#.###.##", 13
    dc.b "..##.##.......#.#......##...#..", 13
    dc.b ".#.....#..##..#.###...#..#.##.#", 13
    dc.b "#.....#.#..#...#........#...#..", 13
    dc.b ".#......#....#.#.....###...#..#", 13
    dc.b "..##.#....#..##......#.....#...", 13
    dc.b "..#.#.##..#.....#.####..###....", 13
    dc.b ".........#......#..#...........", 13
    dc.b "..#........#.##.#.....##.##..#.", 13
    dc.b ".......#.........#....#...#.#..", 13
    dc.b ".##.....#.#....#.#.......#.....", 13
    dc.b "..........#.##........##...##..", 13
    dc.b "###..###.#.#..#..#####.##.#.##.", 13
    dc.b "..##..##.#.#...#..#.#.#......#.", 13
    dc.b "#..#..#..#..##..#.....#......#.", 13
    dc.b "..#....#.##..#......##.........", 13
    dc.b "..#.##......#...##.#......#....", 13
    dc.b ".......#..#.##.#.....#.........", 13
    dc.b ".......#.#.#.###...##......#...", 13
    dc.b ".....#.#..........#..#...#.....", 13
    dc.b "....##..........#..........##..", 13
    dc.b "..#......#.....#.##.#..#...#.#.", 13
    dc.b "....#.....#..#...#..#.#.##..###", 13
    dc.b ".####....#........#...#........", 13
    dc.b "...##.#.##.#..#...##...#.##....", 13
    dc.b "....#...#...#.#.#.#...#..#.....", 13
    dc.b ".....#...#.#.....#.#........##.", 13
    dc.b "..#.#.......###.#.....##.......", 13
    dc.b "......#.........##....#....#..#", 13
    dc.b ".............##.....##.........", 13
    dc.b ".........##...##.......#.....#.", 13
    dc.b "##.........#..........#.###..##", 13
    dc.b "...#.....#......#....#..##.....", 13
    dc.b "##..#...#...##.#.....#.#......#", 13
    dc.b "..#...##.#.......#.#......#.##.", 13
    dc.b "......#.......#.#...........#..", 13
    dc.b "..........#.....##............#", 13
    dc.b "#........#...#..#.......###.##.", 13
    dc.b ".##...........#.#........#.#.#.", 13
    dc.b "...#..##...#.#....#####.#......", 13
    dc.b ".....##...###...#..#.##...####.", 13
    dc.b "...#....#.....#..#.......#.....", 13
    dc.b "#....#....#...#..#..#.######..#", 13
    dc.b "#.###...........#......#...#..#", 13
    dc.b ".#.#.#.#..#....#....#...##.#...", 13
    dc.b ".#..#.........#.#....###...#...", 13
    dc.b "......#..##.##..........#....##", 13
    dc.b ".....#......##....##.....#...#.", 13
    dc.b ".#...#.#.#....##....#..#....#.#", 13
    dc.b "..................#..###.#..##.", 13
    dc.b "..#.........#......#....#..###.", 13
    dc.b "#.#.....#..#..#....###..###....", 13
    dc.b "..##..##.#..##........##...##..", 13
    dc.b "##..#........##..###..#.....#.#", 13
    dc.b "..#..###..#......#....#...#...#", 13
    dc.b "#..#.#..............##.#..#.#..", 13
    dc.b ".....####....#...####.....#.#..", 13
    dc.b ".....#....##.#......###........", 13
    dc.b "##.##...#.#.#.#.......#....##..", 13
    dc.b ".#......#...#.#....#..##.#.##.#", 13
    dc.b "#.#.##.#.#......#..##........##", 13
    dc.b "...##.....#.....#...#..###...#.", 13
    dc.b "........###.....#.....#...##..#", 13
    dc.b ".....#.##.##......#.#....#...#.", 13
    dc.b ".#....##.......#..#.####.......", 13
    dc.b ".#..#....#..........#......#.#.", 13
    dc.b ".#.##.##.....###.#.#...........", 13
    dc.b ".........#......#..##..........", 13
    dc.b "....#...##.#.#.#..#.#.........#", 13
    dc.b "..#.....#.##...#..#..#.###....#", 13
    dc.b "...#.##......#.....##....#.....", 13
    dc.b "###............#.#....#...#....", 13
    dc.b ".......#.....#..#.#.#....#..#.#", 13
    dc.b "...#......#.#..##..#....#...#.#", 13
    dc.b "............##........##..##...", 13
    dc.b "..#..#.##..#......###..#.......", 13
    dc.b "........#.........#............", 13
    dc.b "..#...#.#########.#...##..###..", 13
    dc.b "#....#......#.......#.#.....#..", 13
    dc.b "#.#..#....###.###....#...#.#...", 13
    dc.b "#...###.#.#.......#.##......#..", 13
    dc.b ".................#...#.#.#.....", 13
    dc.b "##....#...#........#....#.#..#.", 13
    dc.b "......#.....#...#..........#.#.", 13
    dc.b "##..........#...#..........#.##", 13
    dc.b "..#.#.##.#....#.#......#...##..", 13
    dc.b ".....#.......#..#.....#........", 13
    dc.b "#.##.#..##..#.......##.........", 13
    dc.b "....#......#..#..#.#...#.......", 13
    dc.b "...#....#................###...", 13
    dc.b ".##.....#.#....#.#..........##.", 13
    dc.b "...#..#....#.##.##......#......", 13
    dc.b "..#.#....#.......#.#..##.......", 13
    dc.b "....#.....#..........##.#.#####", 13
    dc.b "#.....................##..#..#.", 13
    dc.b ".###..#.##.......##.#...#..#...", 13
    dc.b "...###.......#..#...#......#..#", 13
    dc.b "#..#...#.#..#.#..#..#.##.......", 13
    dc.b "#...##.......#..#..#.##..###...", 13
    dc.b "......#....#.#.#........#.##..#", 13
    dc.b "..##..#....#....#..#.#..#......", 13
    dc.b "..##.#...#.#######..#...#.....#", 13
    dc.b "..#....#..#.........#..##......", 13
    dc.b "...#....#.#......#..#..#.#.....", 13
    dc.b "#..#....#........#.#..##....###", 13
    dc.b "#....#..##......##.##.....#.###", 13
    dc.b "...#.#..........#..#.#.#.#.##..", 13
    dc.b "......##..#.#..#.#....#....#...", 13
    dc.b "##....#....#..#..#.##......#...", 13
    dc.b "....#.#..##.#.#...###....##.#..", 13
    dc.b "...#.......##..#.......#...#...", 13
    dc.b "......##.......#..##.....#...#.", 13
    dc.b "...#.#...#...........#...#.....", 13
    dc.b ".#....#...#......##.##..###..#.", 13
    dc.b ".#..........#...#...#...##.##..", 13
    dc.b ".....###..#.....#..##....#.####", 13
    dc.b "..#.###..#..##..##.....#.#.....", 13
    dc.b ".............#.###...##.#.....#", 13
    dc.b "....###.......###.#.....#..#.#.", 13
    dc.b "........##.#.........#.....###.", 13
    dc.b ".....###.#..#.....#...#..#.....", 13
    dc.b ".#....#..##.#..#.#....#.......#", 13
    dc.b "........#......#.#..#.#..#...##", 13
    dc.b "...#.##.##......#..............", 13
    dc.b ".#.....##.#.....#..#......##...", 13
    dc.b "#..#..#.....#.....#.....###....", 13
    dc.b ".##...........#..#.##.....#....", 13
    dc.b "..#.#......#.#...#.##.#..#...##", 13
    dc.b "...#..........#.....#..........", 13
    dc.b "#.#.#.#.#...#....#...#.....##..", 13
    dc.b "#......##...#...#..........#.#.", 13
    dc.b "....##........#.#..............", 13
    dc.b "#..#.#.#..#........##......#.##", 13
    dc.b "........####...##.#.....#......", 13
    dc.b "....#........#.#..#..##..#.#...", 13
    dc.b ".#.....#..###...#..#.....#..#..", 13
    dc.b "#......###.#..#....#..#.#......", 13
    dc.b "....#.....##.##..#...#.#..##.#.", 13
    dc.b "..##..#...#.#......#....#...#.#", 13
    dc.b "#..##...##..#...###...#..#.....", 13
    dc.b ".......#.....#...........##....", 13
    dc.b "#..##....#........#....##..#.#.", 13
    dc.b ".#........#..##...###.#..#.....", 13
    dc.b ".#.#....#..##...#...##.#..###..", 13
    dc.b "#.........#.......#.....#.#....", 13
    dc.b "#..#.....#.#.###.#..#......#...", 13
    dc.b "....#..#.#....#..##..###....###", 13
    dc.b "###.##.#.#..#...........#.#.#..", 13
    dc.b "..##.#.......#......#..##....#.", 13
    dc.b ".....#.#.#.......##.......#...#", 13
    dc.b "...........#.##....##.##....#.#", 13
    dc.b "...#.......#..#.##..#......#..#", 13
    dc.b "#.#.#...#......##.#...........#", 13
    dc.b "##........#...........###.#..#.", 13
    dc.b "..........#.#.#....#.#..##.#.#.", 13
    dc.b "...#.#.#....#..........#..#....", 13
    dc.b "#.#....###.#.#..#.......###...#", 13
    dc.b ".#....#......#.#.#..#..#.......", 13
    dc.b "......##.............#....#.#.#", 13
    dc.b ".#..........#.........#.##.....", 13
    dc.b "##....#....##....#..#.......#..", 13
    dc.b "#.##.##.#..#..#.....#..#.##.#..", 13
    dc.b ".#..#.......##..#.....##.##....", 13
    dc.b ".......#..........#.#.##..#.##.", 13
    dc.b "....#.....#.#...##....##.......", 13
    dc.b ".......#.........#...##....##.#", 13
    dc.b "#.....#......#..........#...#..", 13
    dc.b "...#.#.......#.#..#....###..#..", 13
    dc.b ".....#.#.#.........#...........", 13
    dc.b ".#..###.#.#........#.#.........", 13
    dc.b ".........#..#......##...##....#", 13
    dc.b "...###..#.....##.....#.###....#", 13
    dc.b ".##...#...#........###.#..#....", 13
    dc.b ".##........#..#.###.######.##.#", 13
    dc.b "##.#...#.#....#..##.#....##....", 13
    dc.b ".......##.....##.#..###.#......", 13
    dc.b "..##...##........#.......#....#", 13
    dc.b "#..##...#.####...###......#...#", 13
    dc.b ".##.....#.##.#.#.....###.#..##.", 13
    dc.b "..###....#.#.###.#....#........", 13
    dc.b "....#..###..#...#....#..#..#.#.", 13
    dc.b "#.#.##....##...##.......#......", 13
    dc.b ".........#...#....#..#.........", 13
    dc.b ".............#...#..##.#.......", 13
    dc.b "...#.##.......#...#.#..##.##...", 13
    dc.b ".####.#.##..#.#......#.##...#.#", 13
    dc.b ".#..#.#.....#.................#", 13
    dc.b "..#.##..###....#...#......####.", 13
    dc.b "..##..##...........#....#...#..", 13
    dc.b "....#...#...#...#.......#....#.", 13
    dc.b "#.#...###...#...#.#...#....##.#", 13
    dc.b "......#...#.#.......#.....#...#", 13
    dc.b "....##...#.#.#....#....#.#....#", 13
    dc.b ".....#.....#...##..#...#....##.", 13
    dc.b "#.....#....#......##.##....#...", 13
    dc.b "...#.#....#...#....#.#....##..#", 13
    dc.b "...#.#..#...##....###..#.......", 13
    dc.b "...##......###...###.#...#..#..", 13
    dc.b "##.......#.......###.......#..#", 13
    dc.b "..##.##..###.#............#...#", 13
    dc.b "#.....##..#..##....##..#.......", 13
    dc.b "......#.#...#......#.....#.....", 13
    dc.b "#...........#....#..##.##.#....", 13
    dc.b ".......#..#......#...#....#...#", 13
    dc.b ".#...##...........#......#...#.", 13
    dc.b "#........#....##...###.#....#..", 13
    dc.b ".....#.......##.........#.##...", 13
    dc.b ".#.###..#....#..##.#..#.#..#...", 13
    dc.b "#.......#.##.#.#....#.#..#....#", 13
    dc.b "###.....#.#.......#..#......#.#", 13
    dc.b "#..#.#.......#.#..##..##.#.#...", 13
    dc.b "#..#.#.#.###........#.....#...#", 13
    dc.b "#.#.#..#..##.....#...........#.", 13
    dc.b "..#.#..#.....#...#...#...##....", 13
    dc.b "...#.##......#...##.#...#.#.#.#", 13
    dc.b "#..#.#.#.#.......####..........", 13
    dc.b "..#......#.#......##.###.....##", 13
    dc.b "..#...##..#.........##....#.##.", 13
    dc.b "##.##.##.#.#.....#..........##.", 13
    dc.b ".#.....###.#..#....#..#.###...#", 13
    dc.b "#...##.......###....#.#..#.....", 13
    dc.b "..#....##.........##.........##", 13
    dc.b "......#....#.##.......#........", 13
    dc.b "..#.#.#..#...#...#...##.#...#..", 13
    dc.b "......#..##.#.#.#...##...#.#.##", 13
    dc.b "#..#...##.#.....#...#.##.......", 13
    dc.b "..#..#.........##.#...#.##...##", 13
    dc.b "##.##.#....#.......#.##..#.....", 13
    dc.b ".....##...##.##...##.........##", 13
    dc.b "#......#...#.......#...#...#...", 13
    dc.b "...##...........#...#..#.......", 13
    dc.b ".#.##.#..#........#....#.......", 13
    dc.b "#.#...#..#......##...#.#.##....", 13
    dc.b "##........####..#.#...#.#.##.##", 13
    dc.b "#..#.#.##......##.#.#..#.......", 13
    dc.b ".....#.........#..#.####....#..", 13
    dc.b "......##..#....#...#.#....#....", 13
    dc.b "#...##........#.........#.....#", 13
    dc.b ".#.#...#.#.#..#............##.#", 13
    dc.b ".#..#....#....#.....#...#.....#", 13
    dc.b "..###...#..#.....#.##.###...#.#", 13
    dc.b ".#.###..#..#...#.#...#.#......#", 13
    dc.b "#...#####......###........##...", 13
    dc.b ".....#.....#..#.#....#..##.....", 13
    dc.b "....##...#.#.##.#####...#....#.", 13
    dc.b ".#.#.........##.#.......#..##..", 13
    dc.b ".#...#.#...#...#....#.#...##.#.", 13
    dc.b ".##...#..#.#..#......#.#.#..##.", 13
    dc.b "..#.....#..#.....##.....#......", 13
    dc.b "..#........#..##...#.......###.", 13
    dc.b ".#....#.......#....#....#..#...", 13
    dc.b "....#......#.#.#.........#.....", 13
    dc.b "..##...#.#.#...#.#........#....", 13
    dc.b ".#.....####...##.#..#...##.....", 13
    dc.b "...#.....#...#...#....#....#...", 13
    dc.b ".........#..#.#.....#..#.#..#..", 13
    dc.b ".........##...........#.......#", 13
    dc.b "......#..#.....##...#.##.#.....", 13
    dc.b ".#......##........##...#.#.##..", 13
    dc.b ".....#.#..##...........#..#..#.", 13
    dc.b "...#.......#...#.#..#.##..#.##.", 13
    dc.b "...#.......#.....#.#...#.##.#..", 13
    dc.b "#.....#.............##.#..####.", 13
    dc.b ".#...#......#...##.#....#.#....", 13
    dc.b ".##..##.##....#.#.....#.......#", 13
    dc.b "...#...#....#....##.#..#....##.", 13
    dc.b "..............##....#.......#.#", 13
    dc.b ".#.#.#...##..#..#...###.#..#...", 13
    dc.b ".#.#...#.#..#.#..#...######..#.", 13
    dc.b "........#......#.#..#.#....#...", 13
    dc.b "..###.....###.#.##....#...##...", 13
    dc.b ".##.#.....#.......##.......#...", 13
    dc.b "..#..##...#..........#.#....#.#", 13,0
INPUTEND: