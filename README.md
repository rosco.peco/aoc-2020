# AoC 2020 - M68k assembly language

I decided to have a crack at AoC this year in M68k assembly language, because, well, why not?

I seriously doubt I'll do all the challenges (they tend to get a bit too time-consuming
for my schedule even in high level languages and won't all suit assembly too well I'd bet)
but I'll do as many as I can find the time for :) 

You can probably run these in MAME or Easy68k...

