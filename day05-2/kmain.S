; *************************************************************
; Advent Of Code 2020 Day 05 - Part 1
; Copyright (c) 2020 Ross Bamford (roscopeco@gmail.com)
;
; Part two is similar to part 1. All we do this time is
; populate a bitmap with the seats we see as we go, then
; spool through the map to find the first empty seat with
; a -1 neighbour.
; *************************************************************
    section .text                     ; This is normal code

kmain::
    movem.l D0-D2/A0,-(A7)            ; Save regs
    lea     INPUT,A0                  ; Load input
    move.l  #0,D1                     ; Highest starts at 0

.LOOP
    move.b  (A0),D2
    tst.b   d2                        ; Hit null terminator?
    beq.s   .DONE                     ; We're done if so

    bsr.s   CALCSEAT                  ; Calc seat number
    bsr.s   MARKSEAT                  ; Mark this seat in the bitmap

    cmp.w   D1,D0                     ; Is result higher than previous?
    bls.s   .LOOP                     ; No - Just continue looping

    move.w  D0,D1                     ; Else, set new highest

    bra.s   .LOOP

.DONE
    bsr.w   FINDSEAT
    
    move.w  D0,D1                     ; Display our sear
    moveq.l #15,D0
    moveq.l #10,D2
    trap    #15

    movem.l (A7)+,D0-D2/A0            ; Restore regs
    rts


; Calculate a seat given an input string in the puzzle format.
;
; Arguments:
;   A0  - String pointer
;
; Modifies:
;   D0.W- Result
;   A0  - Points to next string (will skip CR if it's there)
;
CALCSEAT:
    movem.l D1-D2,-(A7)               ; Save regs
 
    clr.l   D0                        ; Col starts at 0
    clr.l   D1                        ; Row starts at 0

    ; Compute row first
    moveq.l #6,D2                     ; Loop counter at 6 for row
    bsr.s   CALCONE                   ; Calculate it
    move.b  D0,D1                     ; Row into D1

    ; Compute column
    moveq.l #2,D2                     ; Loop counter at 2 for column
    bsr.s   CALCONE                   ; And calculate it

    mulu.w  #8,D1                     ; Mul row by 8
    add.w   D1,D0                     ; Add to column

    move.b  (A0),D1                   ; Check for CR...
    cmp.b   #13,D1                    ; Do we have it?
    bne.s   .DONE                     ; Nope - just leave

    addq.l  #1,A0                     ; Else, skip it.

.DONE
    movem.l (A7)+,D1-D2               ; Restore regs
    rts


; Calculate either row or column depending on input position
; and loop count. Use count 6 at start of string for row, 
; count 2 at first L or R for column.
;
; Calculation is just simple bit shifting and addition..
;
; Arguments:
;   A0   - String pointer
;   D2.B - Loop count
;
; Modifies
;   D0.B - Result
;   A0   - Points to next character
;
CALCONE:
    movem.l D1-D2,-(A7)               ; Save regs
    clr.b   D0                        ; Clear D0

.LOOP
    move.b  (A0)+,D1                  ; Get next character
    cmp.b   #'B',D1                   ; Is it 'B'?
    beq.s   .UPPER                    ; Yes - branch

    cmp.b   #'R',D1                   ; Is it 'R'?
    beq.s   .UPPER                    ; Yes - shift and add
    bra.s   .NEXT                     ; Else next iteration (F or L, insignificant)

.UPPER
    move.b  #1,D1                     ; It's a B or R, so it's significant
    lsl.b   D2,D1                     ; Logical shift left by (counter) bits
    add.b   D1,D0                     ; Add to result

.NEXT
    cmp.b   #0,D2                     ; Was this the last iteration?
    beq.s   .DONE                     ; Break if so
    
    subi.b  #1,D2                     ; Else decrement
    bra.s   .LOOP                     ; and loop 

.DONE
    movem.l (A7)+,D1-D2               ; Restore regs
    rts

; Mark a given seat (from D0) in the bitmap as used. 
;
; Arguments
;   D0.W  - Seat
;   
; Modifies
;   Bitmap (BM)
;   No registers
;
MARKSEAT:
    movem.l D0-D2/A0,-(A7)            ; Save regs

    clr.l   D1                        ; Zero D1 long.
    move.w  D0,D1                     ; Get current seat number
    lsr.w   #5,D1                     ; Divide by 32 to get the appropriate long
    lsl.w   #2,D1                     ; And multiply by four for byte address

    lea     BM,A0                     ; ... and use it to point to our bitmap
    add.l   D1,A0                     ; (with appropriate offset)

    lsr.w   #2,D1                     ; Make D1 the long address again.

    clr.l   D2                        ; Clear D2...
    move.b  #32,D2                    ; ... And move 32 into D2
    mulu.w  D1,D2                     ; ... Multiply by 32
    sub.w   D2,D0                     ; ... Then subtract from value to get bit

    move.l  (A0),D1                   ; Grab the long for a 32-bit op
    bset.l  D0,D1                     ; Set the bit...
    move.l  D1,(A0)                   ; And move it back to memory

    movem.l (A7)+,D0-D2/A0            ; Restore regs
    rts                               ; And done.

; Go through the bitmap, finding the first empty seat with two neighbours.
; Not terribly efficient (it just loops the bits) but it'll do for now...
;
; Requires that the bitmap has already been set up!
;
; Arguments:
;   None
;
; Modifies:
;   D0.W  - Our seat number, or 0 if not found.
;
FINDSEAT:
    movem.l D1-D4/A0,-(A7)            ; Save regs

    move.b  #0,D3                     ; Flag to determine if we're past empty seats
    clr.w   D4                        ; Loop counter starts at 0

.FINDLOOP
    cmp.w   #1024,D4                  ; Exhausted iterations?
    beq     .FINDFAILED               ; Failed if so :( 

    clr.l   D0                        ; Zero D0 long.
    clr.l   D1                        ; Zero D1 long.
 
    move.w  D4,D1                     ; Get current seat number
    lsr.w   #5,D1                     ; Divide by 32 to get the appropriate long
    lsl.w   #2,D1                     ; And multiply by four for byte address

    lea     BM,A0                     ; ... and use it to point to our bitmap
    add.l   D1,A0                     ; (with appropriate offset)

    lsr.w   #2,D1                     ; Make D1 the long address again.

    clr.l   D2                        ; Clear D2...
    move.b  #32,D2                    ; ... And move 32 into D2
    mulu.w  D1,D2                     ; ... Multiply by 32
    move.w  D4,D0                     ; Seat number into D0 again for modification...
    sub.w   D2,D0                     ; ... by subtracting D2 from it to get bit

    move.l  (A0),D1                   ; Grab the long for a 32-bit op
    btst.l  D0,D1                     ; Check the bit...
    beq.s   .NOTSET                   ; Branch if it's clear

    move.b  #1,D3                     ; Set flag to indicate past empty seats
    bra.s   .FINDNEXT                 ; And continue loop

.NOTSET
    tst.b   D3                        ; Are we past empty seats already?
    beq.s   .FINDNEXT                 ; Just carry on looping if not...

    ; If we're here, we found it. D4 contains our seat.
    move.w  D4,D0                     ; Move result into return register
    bra.s   .DONE                     ; And leave.

.FINDNEXT
    addi.w  #1,D4                     ; Increment loop counter
    bra     .FINDLOOP                 ; And loop.

.FINDFAILED
    ; If we get here, something's wrong - we didn't find it!
    ; According to the puzzle spec, 0 is a valid Error value as it can't
    ; have two occupied neighbours...
    clr.l   D0

.DONE
    movem.l (A7)+,D1-D4/A0            ; Save regs
    rts


    section .data
BM  ds.l  32                          ; Bitmap is 32 longwords (128 bytes, 1024 bits)
    
    section .rodata
    include 'input.inc'

