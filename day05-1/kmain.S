; *************************************************************
; Advent Of Code 2020 Day 05 - Part 1
; Copyright (c) 2020 Ross Bamford (roscopeco@gmail.com)
;
; Nice easy one for today, just a bit of bit-shifting...
; Kind of a nice break after the madness of Regex engines :D
; *************************************************************
    section .text                     ; This is normal code

kmain::
    movem.l D0-D2/A0,-(A7)            ; Save regs
    lea     INPUT,A0                  ; Load input
    move.l  #0,D1                     ; Highest starts at 0

.LOOP
    move.b  (A0),D2
    tst.b   d2                        ; Hit null terminator?
    beq.s   .DONE                     ; We're done if so

    bsr.s   CALCSEAT                  ; Calc seat number

    cmp.w   D1,D0                     ; Is result higher than previous?
    bls.s   .LOOP                     ; No - Just continue looping

    move.w  D0,D1                     ; Else, set new highest

    bra.s   .LOOP

.DONE
    moveq.l #15,D0                    ; Display highest ID
    moveq.l #10,D2
    trap    #15

    movem.l (A7)+,D0-D2/A0            ; Restore regs
    rts


; Calculate a seat given an input string in the puzzle format.
;
; Arguments:
;   A0  - String pointer
;
; Modifies:
;   D0.W- Result
;   A0  - Points to next string (will skip CR if it's there)
;
CALCSEAT:
    movem.l D1-D2,-(A7)               ; Save regs
 
    clr.l   D0                        ; Col starts at 0
    clr.l   D1                        ; Row starts at 0

    ; Compute row first
    moveq.l #6,D2                     ; Loop counter at 6 for row
    bsr.s   CALCONE                   ; Calculate it
    move.b  D0,D1                     ; Row into D1

    ; Compute column
    moveq.l #2,D2                     ; Loop counter at 2 for column
    bsr.s   CALCONE                   ; And calculate it

    mulu.w  #8,D1                     ; Mul row by 8
    add.w   D1,D0                     ; Add to column

    move.b  (A0),D1                   ; Check for CR...
    cmp.b   #13,D1                    ; Do we have it?
    bne.s   .DONE                     ; Nope - just leave

    addq.l  #1,A0                     ; Else, skip it.

.DONE
    movem.l (A7)+,D1-D2               ; Restore regs
    rts


; Calculate either row or column depending on input position
; and loop count. Use count 6 at start of string for row, 
; count 2 at first L or R for column.
;
; Calculation is just simple bit shifting and addition..
;
; Arguments:
;   A0   - String pointer
;   D2.B - Loop count
;
; Modifies
;   D0.B - Result
;   A0   - Points to next character
;
CALCONE:
    movem.l D1-D2,-(A7)               ; Save regs
    clr.b   D0                        ; Clear D0

.LOOP
    move.b  (A0)+,D1                  ; Get next character
    cmp.b   #'B',D1                   ; Is it 'B'?
    beq.s   .UPPER                    ; Yes - branch

    cmp.b   #'R',D1                   ; Is it 'R'?
    beq.s   .UPPER                    ; Yes - shift and add
    bra.s   .NEXT                     ; Else next iteration (F or L, insignificant)

.UPPER
    move.b  #1,D1                     ; It's a B or R, so it's significant
    lsl.b   D2,D1                     ; Logical shift left by (counter) bits
    add.b   D1,D0                     ; Add to result

.NEXT
    cmp.b   #0,D2                     ; Was this the last iteration?
    beq.s   .DONE                     ; Break if so
    
    subi.b  #1,D2                     ; Else decrement
    bra.s   .LOOP                     ; and loop 

.DONE
    movem.l (A7)+,D1-D2               ; Restore regs
    rts

    section .data
    align 2
    include 'input.inc'

